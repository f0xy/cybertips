import sys
from app import api_manager
from app.database import db
from app.database.models.user import User
from app.database.models.admin import Admin
from app.database.models.prediction import Prediction
from app.database.models.comment import Comment
from app.database.models.notification import Notification
from app.database.models.timepoint import TimePoint
from app.database.models.eventsliga import EventsLiga
from app.database.models.eventsteams import EventsTeams
from app.database.models.comment import Comment
from app.database.models.subscribers import Subscribe
from app.database.models.statistic import Statistic
from app.database.models.express import Express
from app.database.models.likes import Like
from app.database.models.views import View


v1_api_url_prefix = '/api/v1'

# def num_of_comments(result, **kw):
#     for obj in result['objects']:
#         obj.setdefault('num_of_comments', Comment.query.filter(Comment.prediction_id == obj['id']).count())
#         print(result, flush=True)

def user_nickname(result, **kw):
    for obj in result['objects']:
        obj.setdefault('nickname', User.query.filter_by(id = obj['user_id']).first().nickname)
        obj.setdefault('user_img', User.query.filter_by(id = obj['user_id']).first().user_img)
        
    
# postprocessors = {'GET_MANY': [num_of_comments]}
process_user_nickname = {'GET_MANY': [user_nickname]}


api_manager.create_api(User, url_prefix=v1_api_url_prefix, methods=['GET', 'POST', 'PATCH'])
api_manager.create_api(Admin, url_prefix=v1_api_url_prefix, methods=['GET', 'POST'])

api_manager.create_api(Prediction, url_prefix=v1_api_url_prefix, methods=['GET', 'POST', 'PATCH'])
api_manager.create_api(Express, url_prefix=v1_api_url_prefix, methods=['GET', 'POST', 'PATCH'])

api_manager.create_api(Comment, url_prefix=v1_api_url_prefix, methods=['GET', 'POST'], results_per_page=10000, max_results_per_page=1000000)
api_manager.create_api(Notification, url_prefix=v1_api_url_prefix, methods=['GET', 'POST', 'DELETE'], allow_delete_many=True)
api_manager.create_api(Subscribe, url_prefix=v1_api_url_prefix, methods=['GET', 'POST', 'DELETE'], allow_delete_many=True)
api_manager.create_api(Statistic, postprocessors=process_user_nickname, url_prefix=v1_api_url_prefix, methods=['GET', 'POST', 'DELETE'], allow_delete_many=True)

api_manager.create_api(Like, url_prefix=v1_api_url_prefix, methods=['GET', 'POST', 'DELETE'], allow_delete_many=True)
api_manager.create_api(View, url_prefix=v1_api_url_prefix, methods=['GET', 'POST', 'DELETE'], allow_delete_many=True)



# Api для определения когда матч завершится
api_manager.create_api(TimePoint, url_prefix=v1_api_url_prefix, methods=['GET', 'POST', 'DELETE'], allow_delete_many=True)
api_manager.create_api(EventsTeams, url_prefix=v1_api_url_prefix, methods=['GET', 'POST', 'DELETE'], allow_delete_many=True)
api_manager.create_api(EventsLiga, url_prefix=v1_api_url_prefix, methods=['GET', 'POST', 'DELETE'], allow_delete_many=True)
