from app.database import db
from sqlalchemy import ForeignKey


class Subscribe(db.Model):
    __tablename__ = 'subscribers'

    id = db.Column(db.Integer, primary_key= True)
    subscription_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    subscriber_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __repr__(self):
        return "<Teams(id = '{0}', subscription = '{1}', subscriber = '{2}')>".format(self.id, self.subscription_id, self.subscriber_id)