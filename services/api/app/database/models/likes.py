from app.database import db
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref


class Like(db.Model):
    __tablename__ = 'likes'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    prediction_id = db.Column(db.Integer, db.ForeignKey('predictions.id'), nullable=False)
    

    def __repr__(self):
        return "<User(id_user = '{0}', id_prediction = '{1}')>".format(self.user_id, self.prediction_id)