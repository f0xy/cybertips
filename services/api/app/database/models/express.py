from app.database import db
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship


class Express(db.Model): 
    __tablename__ = 'express'

    id = db.Column(db.Integer, primary_key=True)
    prediction_id = db.Column(db.Integer, db.ForeignKey('predictions.id'), nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    liga = db.Column(db.String(200), nullable=False)
    game_start_time = db.Column(db.DateTime)

    team1 = db.Column(db.String(120), nullable=False)
    team2 = db.Column(db.String(120), nullable=False)

    team1_img = db.Column(db.String(400), nullable=False)
    team2_img = db.Column(db.String(400), nullable=False)

    type_of_prediction = db.Column(db.String(120), nullable=False) 
    coefficient = db.Column(db.Float, nullable=False)

    status_id = db.Column(db.Integer, db.ForeignKey('predictionstatuses.index'), nullable=False)
    status = db.relationship('PredictionStatuses')

    type_id = db.Column(db.Integer, db.ForeignKey('predictiontypes.index'), nullable=False)
    type_ = relationship('PredictionTypes')

    game_1x_id = db.Column(db.Integer)
    liga_1x_id = db.Column(db.Integer)

    
    def __repr__(self):
        return "<Express(id = '{0}', prediction_id = '{1}')>".format(self.id, self.prediction_id)
