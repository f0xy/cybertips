from app.database import db


class PredictionTypes(db.Model):
    __tablename__ = 'predictiontypes'

    id = db.Column(db.Integer, primary_key= True)
    index = db.Column(db.Integer, unique=True)
    tag = db.Column(db.String(100))
    

    def __repr__(self):
        return "<PredictionStatuses(index = '{0}', tag = '{1}')>".format(self.index, self.tag)
