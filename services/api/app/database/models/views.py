from app.database import db
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref


class View(db.Model):
    __tablename__ = 'views'

    id = db.Column(db.Integer, primary_key=True)
    user_ip = db.Column(db.String(20))
    prediction_id = db.Column(db.Integer, db.ForeignKey('predictions.id'), nullable=False)
    

    def __repr__(self):
        return "<User(id_user = '{0}', id_prediction = '{1}')>".format(self.user_ip, self.prediction_id)