from app.database import db
from sqlalchemy import ForeignKey


class EventsTeams(db.Model):
    __tablename__ = 'eventsteams'

    id = db.Column(db.Integer, primary_key= True)
    liga_id = db.Column(db.Integer, db.ForeignKey('eventsliga.id'))
    team1 = db.Column(db.String(200))
    team2 = db.Column(db.String(200))
    

    def __repr__(self):
        return "<Teams(id = '{0}', liga_id = '{1}', team1 = '{2}', team2 = '{3}')>".format(self.id, self.liga_id, self.team1, self.team2)