from app.database import db
from sqlalchemy import ForeignKey

class Comment(db.Model): 
    __tablename__ = 'comments'

    id = db.Column(db.Integer, primary_key= True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable = False)
    date = db.Column(db.DateTime, nullable = False)
    description = db.Column(db.String(350), nullable = False)
    prediction_id = db.Column(db.Integer, db.ForeignKey('predictions.id'), nullable = False)
    reply_id = db.Column(db.Integer)
    user_details = db.relationship('User')

    

    def __repr__(self):
        return "<Comment(id = '{0}', user_id = '{1}', prediction_id = '{2}')>".format(self.id, self.user_id, self.prediction_id)