import sys
import traceback
from app import application
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


db = SQLAlchemy(application)
migrate = Migrate(application, db)

from .models import (
    admin,
    prediction,
    user,
    comment,
    notification,
    timepoint,
    eventsliga,
    eventsteams,
    predictionstatuses,
    predictiontypes,
    subscribers,
    statistic,
    express,
    likes,
    views
)

# Создает дефолтные статусы и типы (Если нет миграции с этими моделями то потребуется перезапустить
# контейнер сразу после успешной миграции)
# !!! НЕ ТРОГАТЬ индексы менять можно только теги
from .models.predictionstatuses import PredictionStatuses
from .models.predictiontypes import PredictionTypes

try:
    statuses = PredictionStatuses.query.all()
    types = PredictionTypes.query.all()

    if len(statuses) != 6:

        prediction_statuses = [PredictionStatuses(index=0, tag='Создан'), PredictionStatuses(index=1, tag='В лайве'),
                               PredictionStatuses(index=2, tag='Зашел'), PredictionStatuses(index=3, tag='Не зашел'),
                               PredictionStatuses(index=4, tag='Матч оказался в составе экспресса который не сыграл'),
                               PredictionStatuses(index=5, tag="Возврат ставки")]

        for i in range(len(statuses), len(prediction_statuses)):
            db.session.add(prediction_statuses[i])

        db.session.commit()

    if len(types) != 3:
        prediction_types = [PredictionTypes(index=0, tag='Обычный'), PredictionTypes(index=1, tag='Экспресс'),
                            PredictionTypes(index=2, tag='Лайв')]

        for i in range(len(types), len(prediction_types)):
            db.session.add(prediction_types[i])

        db.session.commit()

    
except:
    traceback.print_exc()
    for i in range(8):
        print('MAKE MIGRATE AND RESTART CONTAINERS MAKE MIGRATE AND RESTART CONTAINERS\nMAKE MIGRATE AND RESTART CONTAINERS MAKE MIGRATE AND RESTART CONTAINERS\nMAKE MIGRATE AND RESTART CONTAINERS MAKE MIGRATE AND RESTART CONTAINERS\nMAKE MIGRATE AND RESTART CONTAINERS MAKE MIGRATE AND RESTART CONTAINERS', file=sys.stdout)  
