from models.user import User
from models.prediction import Prediction  # Not used but needed for mapper
from models.predictionstatuses import PredictionStatuses  # Not used but needed for mapper


def fora_type_check(prediction, game, session, change_objects):
    main_score = game['scores'][0].split(' ')[0].split(':')
    team1_score = int(main_score[0])
    team2_score = int(main_score[-1])
    fora_team = int(prediction.type_of_prediction.split(' ')[0][-1]) # Получает число 1 или 2 из Ф1 или Ф2
    fora_number = float(prediction.type_of_prediction.split(' ')[-1]) # Получает число идущее после Ф1 или Ф2
    user = session.query(User).filter(User.id == prediction.user_id).first()

    #Ф1
    if fora_team == 1:
        if fora_number == 0:
            if team1_score == team2_score:
                #возврат бабок
                change_objects('refund', user, prediction, session)
                return
            elif team1_score > team2_score:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team1_score < team2_score:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == 1:
            if team1_score == team2_score or team1_score > team2_score:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team1_score - team2_score == -1:
                # возврат бабок
                change_objects('refund', user, prediction, session)
                return
            elif team1_score - team2_score > -1:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == -1:
            if team1_score - team2_score > 1:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team1_score - team2_score == 1:
                # возврат бабок
                change_objects('refund', user, prediction, session)
                return
            elif team1_score - team2_score < 1:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == 1.5:
            if team1_score - team2_score >= -1:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team1_score - team2_score < -1:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == -1.5:
            if team1_score - team2_score >= 2:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team1_score - team2_score < 2:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == 2:
            if team1_score - team2_score >= - 1:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team1_score - team2_score == -2:
                # возврат бабок
                change_objects('refund', user, prediction, session)
                return
            elif team1_score - team2_score < -2:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == -2:
            if team1_score - team2_score > 2:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team1_score - team2_score == 2:
                # возврат бабок
                change_objects('refund', user, prediction, session)
                return
            elif team1_score - team2_score < 2:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == 2.5:
            if team1_score - team2_score > -3:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team1_score - team2_score <= -3:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return 
        elif fora_number == -2.5:
            if team1_score - team2_score >= 3:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team1_score - team2_score < 3:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        else:
            #на этом наши полномочия всё
            pass
    
    # Ф2
    elif fora_team == 2:
        if fora_number == 0:
            if team2_score == team1_score:
                #возврат бабок
                change_objects('refund', user, prediction, session)
                return
            elif team2_score > team1_score:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team2_score < team1_score:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == 1:
            if team2_score == team1_score or team2_score > team1_score:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team2_score - team1_score == -1:
                # возврат бабок
                change_objects('refund', user, prediction, session)
                return
            elif team2_score - team1_score > -1:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == -1:
            if team2_score - team1_score > 1:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team2_score - team1_score == 1:
                # возврат бабок
                change_objects('refund', user, prediction, session)
                return
            elif team2_score - team1_score < 1:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == 1.5:
            if team2_score - team1_score >= -1:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team2_score - team1_score < -1:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == -1.5:
            if team2_score - team1_score >= 2:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team2_score - team1_score < 2:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == 2:
            if team2_score - team1_score >= - 1:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team2_score - team1_score == -2:
                # возврат бабок
                change_objects('refund', user, prediction, session)
                return
            elif team2_score - team1_score < -2:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == -2:
            if team2_score - team1_score > 2:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team2_score - team1_score == 2:
                # возврат бабок
                change_objects('refund', user, prediction, session)
                return
            elif team2_score - team1_score < 2:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == 2.5:
            if team2_score - team1_score > -3:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team2_score - team1_score <= -3:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif fora_number == -2.5:
            if team2_score - team1_score >= 3:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            elif team2_score - team1_score < 3:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        else:
            #на этом наши полномочия всё
            pass
    else:
        # на этом наши полномочия всё
        pass