import sys
from datetime import datetime

from sqlalchemy import and_

from models.statistic import Statistic
from models.user import User # Not used but needed for mapper
from models.prediction import Prediction
from models.express import Express # Not used but needed for mapper
from models.predictionstatuses import PredictionStatuses  # Not used but needed for mapper


def change_objects(result, user, prediction, session):

    def win_statistic_changes(prediction, statistic, session):
        profit = prediction.coefficient * prediction.stake_amount - prediction.stake_amount

        prediction.status_id = 2
        prediction.final_stake = profit

        user.balance += prediction.coefficient * prediction.stake_amount

        statistic.amount_predictions += 1
        statistic.profit += profit
        statistic.stake_amount += prediction.stake_amount

        if statistic.stake_amount != 0:
            statistic.ROI = ( statistic.profit / statistic.stake_amount ) * 100

        statistic.successful_predictions += 1

        session.commit()

    def lose_statistic_changes(prediction, statistic, session):
        profit = -prediction.stake_amount

        prediction.status_id = 3
        prediction.final_stake = profit

        statistic.amount_predictions += 1
        statistic.profit += profit
        statistic.stake_amount += prediction.stake_amount

        if statistic.stake_amount != 0:
            statistic.ROI = ( statistic.profit / statistic.stake_amount ) * 100

        statistic.unsuccessful_predictions += 1

        session.commit()

        predictions = session.query(Prediction).filter(and_(Prediction.user_id == prediction.user_id,
                                                            Prediction.status_id != 0)).all()
        if len(predictions) == 0:
            curr_user = session.query(User).filter(User.id == prediction.user_id).first()
            if curr_user:
                if curr_user.balance < 10:
                    curr_user.amount_credits += 1
                    curr_user.balance = 2000
                    session.commit()
    

    def refund_statistic_changes(prediction, statistic, session):

        prediction.status_id = 5
        prediction.final_stake = 0

        user.balance += prediction.stake_amount

        statistic.amount_predictions += 1
        statistic.stake_amount += prediction.stake_amount


        if statistic.stake_amount != 0:
            statistic.ROI = ( statistic.profit / statistic.stake_amount ) * 100

        session.commit()



    current_date = datetime.now()
    month = current_date.month
    year = current_date.year

    statistic = session.query(Statistic).filter(and_(Statistic.user_id == user.id,
                                                     Statistic.month == month,
                                                     Statistic.year == year)).first()
    if not statistic:
        statistic = Statistic(user_id=user.id, month=month, year=year)
        session.add(statistic)
        session.commit()

        statistic = session.query(Statistic).filter(and_(Statistic.user_id == user.id,
                                                         Statistic.month == month,
                                                         Statistic.year == year)).first()

    # Если обычный матч
    if prediction.type_id == 0:

        if result == 'win':
            win_statistic_changes(prediction, statistic, session)

        if result == 'lose':
            lose_statistic_changes(prediction, statistic, session)
        
        if result == 'refund':
            refund_statistic_changes(prediction, statistic, session)

    # Если экспресс
    if prediction.type_id == 1:

        if result == 'win':

            prediction.status_id = 2
            session.commit()

            parent_prediction = session.query(Prediction).filter(Prediction.id == prediction.prediction_id).first()

            if parent_prediction:
                count = 0
                for express in parent_prediction.express:
                    if express.status_id == 2 or express.status_id == 5:
                        count += 1
                        continue
                    if express.status_id == 3:
                        lose_statistic_changes(parent_prediction, statistic, session)
                        break

                if count == len(parent_prediction.express):
                    win_statistic_changes(parent_prediction, statistic, session)

            else:
                print(f'Ошибка в проверке выигрышного экспресса c id родителя {prediction.prediction_id}: НЕ УДАЛОСЬ ПОЛУЧИТЬ РОДИТЕЛЯ', file=sys.stdout)

        if result == 'lose':
            prediction.status_id = 3
            session.commit()

            parent_prediction = session.query(Prediction).filter(Prediction.id == prediction.prediction_id).first()

            if parent_prediction:
                lose_statistic_changes(parent_prediction, statistic, session)
                for express in parent_prediction.express:
                    if express.status_id == 0:
                        express.status_id = 4
                session.commit()
            else:
                print(f'Ошибка в проверке проигрышного экспресса c id родителя {prediction.prediction_id}: НЕ УДАЛОСЬ ПОЛУЧИТЬ РОДИТЕЛЯ', file=sys.stdout)
        
        if result == 'refund':
            prediction.status_id = 5
            session.commit()

            parent_prediction = session.query(Prediction).filter(Prediction.id == prediction.prediction_id).first()

            if parent_prediction:
                if prediction.coefficient > 0:
                    parent_prediction.coefficient = round(parent_prediction.coefficient / prediction.coefficient, 3)
                    session.commit()
                    
                    count = 0
                    refund_count = 0
                    for express in parent_prediction.express:
                        if express.status_id == 2:
                            count += 1
                            continue
                            
                        if express.status_id == 5:
                            refund_count += 1
                            continue

                        if express.status_id == 3:
                            lose_statistic_changes(parent_prediction, statistic, session)
                            break
                    
                    if refund_count == len(parent_prediction.express):
                        refund_statistic_changes(parent_prediction, statistic, session)

                    elif count + refund_count == len(parent_prediction.express):
                        win_statistic_changes(parent_prediction, statistic, session)

            else:
                print(f'Ошибка в проверке выигрышного экспресса c id родителя {prediction.prediction_id}: НЕ УДАЛОСЬ ПОЛУЧИТЬ РОДИТЕЛЯ', file=sys.stdout)

                 