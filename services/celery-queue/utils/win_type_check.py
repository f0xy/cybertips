from models.user import User
from models.prediction import Prediction  # Not used but needed for mapper
from models.predictionstatuses import PredictionStatuses  # Not used but needed for mapper
win_type = ['П1', 'Ничья', 'П2']


def win_type_check(prediction, game, session, change_objects):
    main_score = game['scores'][0].split(' ')[0].split(':')
    team1_score = int(main_score[0])
    team2_score = int(main_score[-1])
    user = session.query(User).filter(User.id == prediction.user_id).first()

    if prediction.type_of_prediction == win_type[1]:
        if team1_score == team2_score:
            change_objects('win', user, prediction, session)
            return
        else:
            change_objects('lose', user, prediction, session)
            return

    if prediction.type_of_prediction == win_type[0]:
        if team1_score > team2_score:
            change_objects('win', user, prediction, session)
            return
        else:
            change_objects('lose', user, prediction, session)
            return

    if prediction.type_of_prediction == win_type[-1]:
        if team1_score < team2_score:
            change_objects('win', user, prediction, session)
            return
        else:
            change_objects('lose', user, prediction, session)
            return
