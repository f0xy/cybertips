from models.user import User
from models.prediction import Prediction  # Not used but needed for mapper
from models.predictionstatuses import PredictionStatuses  # Not used but needed for mapper


def total_type_check(prediction, game, session, change_objects):
    main_score = game['scores'][0].split(' ')[0].split(':')
    team1_score = int(main_score[0])
    team2_score = int(main_score[-1])
    type_of_total = prediction.type_of_prediction.split(' ')[0] # Получает строку в которой может быть ТБ или ТМ
    total_number = float(prediction.type_of_prediction.split(' ')[-1]) # Получает число идущее после ТБ или ТМ
    user = session.query(User).filter(User.id == prediction.user_id).first()

    #ТМ
    if "ТМ" in type_of_total:
        if total_number == 1.5:
            if 0 <= team1_score + team2_score <= 1:
                #  зашёл
                change_objects('win', user, prediction, session)
                return
            else:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif total_number == 2.5:
            if 0 <= team1_score + team2_score <= 2:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            else:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        else:
            # на этом наши полномочия всё
            pass
    #ТБ
    elif 'ТБ' in type_of_total:
        if total_number == 1.5:
            if team1_score + team2_score >= 2:
                #зашёл
                change_objects('win', user, prediction, session)
                return
            else:
                # не зашёл
                change_objects('lose', user, prediction, session)
                return
        elif total_number == 2.5:
            if team1_score + team2_score >= 3:
                # зашёл
                change_objects('win', user, prediction, session)
                return
            else:
                #не зашёл
                change_objects('lose', user, prediction, session)
                return
    else:
        # на этом наши полномочия всё
        pass




