from models import Base
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer, String, Float, DateTime, Boolean


class Like(Base):
    __tablename__ = 'likes'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    prediction_id = Column(Integer, ForeignKey('predictions.id'), nullable=False)
    

    def __repr__(self):
        return "<User(id_user = '{0}', id_prediction = '{1}')>".format(self.user_id, self.prediction_id)