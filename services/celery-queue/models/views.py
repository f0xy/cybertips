from models import Base
from sqlalchemy import Column
from sqlalchemy import Integer, String, Float, DateTime, Boolean
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref


class View(Base):
    __tablename__ = 'views'

    id = Column(Integer, primary_key=True)
    user_ip = Column(String(20))
    prediction_id = Column(Integer, ForeignKey('predictions.id'), nullable=False)
    

    def __repr__(self):
        return "<User(id_user = '{0}', id_prediction = '{1}')>".format(self.user_ip, self.prediction_id)