from models import Base
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer, String, Float, DateTime, Boolean
from sqlalchemy.orm import relationship

class Comment(Base): 
    __tablename__ = 'comments'

    id = Column(Integer, primary_key= True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable = False)
    date = Column(DateTime, nullable = False)
    description = Column(String(350), nullable = False)
    prediction_id = Column(Integer, ForeignKey('predictions.id'), nullable = False)
    reply_id = Column(Integer)
    user_details = relationship('User')

    

    def __repr__(self):
        return "<Comment(id = '{0}', user_id = '{1}', prediction_id = '{2}')>".format(self.id, self.user_id, self.prediction_id)