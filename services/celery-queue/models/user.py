from models import Base
from sqlalchemy import Column
from sqlalchemy import Integer, String, Float, DateTime, Boolean
from sqlalchemy.orm import relationship


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    email = Column(String(120), unique=True, nullable=False)

    password = Column(String(120), nullable=False)
    nickname = Column(String(120), nullable=False)

    phone = Column(String(120), unique=True)
    date_of_registration = Column(DateTime, nullable=False)

    balance = Column(Float)
    rating = Column(Float)

    total_earn = Column(Integer, nullable=False, default=0)
    amount_predictions = Column(Integer, nullable=False, default=0)

    profit = Column(Integer, nullable=False, default=0)
    ROI = Column(Integer,nullable=False ,default=0)

    successful_predictions = Column(Integer, nullable=False, default=0)
    unsuccessful_predictions = Column(Integer, nullable=False, default=0)

    name = Column(String(120), nullable=True)
    second_name = Column(String(120), nullable=True)

    description = Column(String(240), nullable=True)
    confirmed = Column(Boolean, nullable=False, default=False)

    confirmed_on = Column(DateTime, nullable=True)
    link_vk = Column(String(240), nullable=True)

    link_fb = Column(String(240), nullable=True)
    link_twitter = Column(String(240), nullable=True)
    
    recover_pass = Column(Boolean, nullable=False, default=False)
    user_img = Column(String(240), nullable=False, default='default.svg')

    likes = relationship('Like')
    amount_credits = Column(Integer, nullable=False, default=0)

    def __repr__(self):
        return "<User(id = '{0}', name = '{1}')>".format(self.id, self.name)