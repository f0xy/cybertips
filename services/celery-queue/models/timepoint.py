from models import Base
from sqlalchemy import Column
from sqlalchemy import Integer, String, DateTime, Boolean


class TimePoint(Base):
    __tablename__ = 'timepoints'

    id = Column(Integer, primary_key=True)
    point_for = Column(String(300), nullable=False)
    date = Column(DateTime, nullable=False)
    used = Column(Boolean, default=False)

    def __repr__(self):
        return "<TimePoint(point = '{0}', date = '{1}')>".format(self.point_for, self.date)