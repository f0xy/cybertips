from models import Base
from sqlalchemy import Column
from sqlalchemy import Integer, String
from sqlalchemy import ForeignKey


class EventsTeams(Base):
    __tablename__ = 'eventsteams'

    id = Column(Integer, primary_key=True)
    liga_id = Column(Integer, ForeignKey('eventsliga.id'))
    team1 = Column(String(200))
    team2 = Column(String(200))

    def __repr__(self):
        return "<Teams(id = '{0}', liga_id = '{1}', team1 = '{2}', team2 = '{3}')>".format(self.id, self.liga_id, self.team1, self.team2)