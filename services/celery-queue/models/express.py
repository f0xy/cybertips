from models import Base
from sqlalchemy import ForeignKey
from sqlalchemy import Column
from sqlalchemy import Integer, String, Float, DateTime
from sqlalchemy.orm import relationship


class Express(Base): 
    __tablename__ = 'express'

    id = Column(Integer, primary_key=True)
    prediction_id = Column(Integer, ForeignKey('predictions.id'), nullable=False)

    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)

    liga = Column(String(200), nullable=False)
    game_start_time = Column(DateTime, nullable=False)

    team1 = Column(String(120), nullable=False)
    team2 = Column(String(120), nullable=False)

    team1_img = Column(String(400), nullable=False)
    team2_img = Column(String(400), nullable=False)

    type_of_prediction = Column(String(120), nullable=False) 
    coefficient = Column(Float, nullable=False)

    status_id = Column(Integer, ForeignKey('predictionstatuses.index'), nullable=False)
    status = relationship('PredictionStatuses')

    type_id = Column(Integer, ForeignKey('predictiontypes.index'), nullable=False)
    type_ = relationship('PredictionTypes')

    game_1x_id = Column(Integer)
    liga_1x_id = Column(Integer)

    
    def __repr__(self):
        return "<Express(id = '{0}', prediction_id = '{1}')>".format(self.id, self.prediction_id)
