import os
import time
from datetime import datetime
from celery import Celery
from celery.task.schedules import crontab
from celery.decorators import periodic_task
from check_predictions import check_predictions
from create_statistic import create_statistic


CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL'),
CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND')

celery_app = Celery('tasks', broker=CELERY_BROKER_URL, backend=CELERY_RESULT_BACKEND)


celery_app.conf.beat_schedule = {
    "check_predictions": {
        "task": "tasks.check_predictions",
        "schedule": 300.0
    },

    "monthly_statistics_creation": {
        "task": "tasks.monthly_statistics_creation",
        "schedule": crontab(0, 0, day_of_month='1')
    }
}

@celery_app.task(name='tasks.check_predictions')
def check():
    return check_predictions()

@celery_app.task(name="tasks.monthly_statistics_creation")
def create():
    return create_statistic()
