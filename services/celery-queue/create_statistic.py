# Python_______________________________
import os
import sys
import json
import requests
import time
from datetime import datetime, timedelta

# SqlAlchemy__________________________
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import and_

# Database Models_________________________
from models.timepoint import TimePoint # Not used but needed for mapper
from models.eventsliga import EventsLiga # Not used but needed for mapper
from models.eventsteams import EventsTeams # Not used but needed for mapper
from models.prediction import Prediction # Not used but needed for mapper
from models.express import Express # Not used but needed for mapper
from models.predictionstatuses import PredictionStatuses  # Not used but needed for mapper
from models.predictiontypes import PredictionTypes  # Not used but needed for mapper
from models.likes import Like  # Not used but needed for mapper
from models.user import User 
from models.statistic import Statistic
from models.views import View # Not used but needed for mapper
from models.comments import Comment # Not used but needed for mapper

def create_statistic():
    # SqlAlchemy init__________________________________________________
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    engine = create_engine(SQLALCHEMY_DATABASE_URI)
    session = sessionmaker(bind=engine)()

    # Main variables_______________________________________________________
    current_date = datetime.now()
    month = current_date.month
    year = current_date.year
    user_counter = 0
    statistic_counter = 0

    users = session.query(User).all()
    for user in users:
        user_counter += 1
        user_statistic = session.query(Statistic).filter(and_(Statistic.user_id == user.id,
                                                            Statistic.month == month,
                                                            Statistic.year == year) ).first()
        if not user_statistic:
            statistic_counter += 1
            statistic = Statistic(user_id=user.id, month=month, year=year)
            session.add(statistic)
            session.commit()

    return f'За {month} месяц {year} года, пользователей проверено: {user_counter}, создано статистик {statistic_counter}'
