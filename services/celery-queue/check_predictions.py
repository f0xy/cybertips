# Python_______________________________
import os
import sys
import json
import requests
import time
from datetime import datetime, timedelta

# SqlAlchemy__________________________
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import and_

# Database Models_________________________
from models.timepoint import TimePoint
from models.eventsliga import EventsLiga
from models.eventsteams import EventsTeams
from models.prediction import Prediction
from models.express import Express
from models.predictionstatuses import PredictionStatuses  # Not used but needed for mapper
from models.predictiontypes import PredictionTypes  # Not used but needed for mapper
from models.likes import Like  # Not used but needed for mapper
from models.user import User
from models.statistic import Statistic
from models.views import View # Not used but needed for mapper
from models.comments import Comment # Not used but needed for mapper

# Custom Utils_____________________________________
from utils.change_objects import change_objects
from utils.win_type_check import win_type_check
from utils.total_type_check import total_type_check
from utils.fora_type_check import fora_type_check

def check_predictions():

    # SqlAlchemy init__________________________________________________
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    engine = create_engine(SQLALCHEMY_DATABASE_URI)
    session = sessionmaker(bind=engine)()

    # Main variables_______________________________________________________
    point_for = 'prediction'  # Идентификатор временной точки в базе данных

    moscow_time = timedelta(hours=3)  # Используется для установки Московского времени
    hours_24 = timedelta(hours=24)

    match_results_url = 'https://1xstavka.ru/results/getmain?showAll=true&'
    match_results_headers = {'X-Requested-With': 'XMLHttpRequest'}

    win_type = ['П1', 'Ничья', 'П2']
    total_type = ['ТБ', 'ТМ']

    express_amount = 0
    prediction_amount = 0

    # Пытается запросить последнюю дату проверки у сервера, если ее нет то создает ее
    server_time = session.query(TimePoint).filter(TimePoint.point_for == point_for).first()

    if not server_time:
        current_time = datetime.utcnow()
        timepoint = TimePoint(point_for=point_for, date=current_time)
        session.add(timepoint)
        session.commit()
    else:
        current_time = server_time.date + moscow_time

    def time_interval_creator(timepoint):
        if timepoint:
            dates = []
            timepoint = timepoint.replace(day=timepoint.day-1)
            for _ in range( abs(datetime.utcnow().day - timepoint.day) + 1 ):
                dates.append([timepoint.replace(hour=0, minute=0, second=0), timepoint.replace(day=timepoint.day+1,hour=0, minute=0, second=0)])
                timepoint = timepoint.replace(day=timepoint.day+1)
            return [ [datetime.strftime(time_str, "%Y-%m-%d+%H:%M") for time_str in date ] for date in dates ]
        else:
            raise Exception("Timepoint не был указан")
    
    date_intervals = time_interval_creator(current_time)

    start_date = None
    end_date = None
    if date_intervals:
        start_date = date_intervals[0][0]
        end_date = date_intervals[-1][1]
        for date_interval in date_intervals:
            date = date_interval[0]
            dateTo = date_interval[1]

            result = requests.get(f'{match_results_url}date={date}&dateTo={dateTo}',
                            headers=match_results_headers)

            if result.status_code != 200:
                continue
            else:
                result = json.loads(result.content.decode('utf-8'))["results"]

                # Получаем финальный список со всеми киберспортивными играми
                cyber_games = [block for block in result if block["is_cyber"]][0]['Elems']

                # Получаем все команды которые играют
                events_teams_online = session.query(EventsTeams).all()


                for block_liga in cyber_games:
                    if 'SubSportId' in block_liga and block_liga['SubSportId'] == 1:
                        for game in block_liga['Elems']:
                            for event_game in events_teams_online:
                                if event_game.team1 == game['opp1'] and event_game.team2 == game['opp2']:

                                    print(f'Нашли игру {game["opp1"]} - {game["opp2"]}', file=sys.stdout)

                                    game_date = str(datetime.utcnow().year) + '.' + game['date']

                                    game_date = datetime.strptime(game_date, '%Y.%d.%m %H:%M') - moscow_time

                                    start_time = game_date - timedelta(hours=2.5)
                                    end_time = game_date + timedelta(hours=2.5)

                                    predictions = session.query(Prediction).filter(and_(Prediction.team1 == game['opp1'],
                                                                                        Prediction.team2 == game['opp2'],
                                                                                        Prediction.status_id == 0,
                                                                                        Prediction.type_id == 0,
                                                                                        Prediction.game_start_time >= start_time,
                                                                                        Prediction.game_start_time <= end_time)).all()


                                    expreses = session.query(Express).filter(and_(Express.team1 == game['opp1'],
                                                                                Express.team2 == game['opp2'],
                                                                                Express.status_id == 0,
                                                                                Express.type_id == 1,
                                                                                Express.game_start_time >= start_time,
                                                                                Express.game_start_time <= end_time)).all()
                                                                        
                                    prediction_amount = len(predictions)
                                    express_amount = len(expreses)
                                    

                                    print("Обычных прогнозов:", str(len(predictions)), "Экспрессов:", str(len(expreses)), file=sys.stdout)                                              

                                    if not predictions and not expreses:
                                            continue
                                    else:
                                        print(f'Загрузили игру {game["opp1"]} - {game["opp2"]}', file=sys.stdout)

                                        
                                    # Проверяем обычные матчи
                                    for prediction in predictions:
                                        if prediction.type_of_prediction in win_type:
                                            win_type_check(prediction, game, session, change_objects)
                                            continue

                                        if 'Т' in prediction.type_of_prediction:
                                            total_type_check(prediction, game, session, change_objects)
                                            continue

                                        if 'Ф' in prediction.type_of_prediction:
                                            fora_type_check(prediction, game, session, change_objects)
                                            continue

                                    # Проверяем экспрессы
                                    for express in expreses:
                                        if express.type_of_prediction in win_type:
                                            win_type_check(express, game, session, change_objects)
                                            continue

                                        if 'Т' in express.type_of_prediction:
                                            total_type_check(express, game, session, change_objects)
                                            continue

                                        if 'Ф' in express.type_of_prediction:
                                            fora_type_check(express, game, session, change_objects)
                                            continue

                                    liga_1x_id = predictions[0].liga_1x_id if predictions else expreses[0].liga_1x_id

                                    # Загружаем текущий матч из прослушки
                                    event_team_online = session.query(EventsTeams).filter(and_(EventsTeams.liga_id == liga_1x_id,
                                                                                            EventsTeams.team1 == game['opp1'],
                                                                                            EventsTeams.team2 == game['opp2'])).first()


                                    # Еще раз загружаем текущие матчи чтобы убедиться что у всех изменился статус 
                                    predictions = session.query(Prediction).filter(and_(Prediction.team1 == game['opp1'],
                                                                                        Prediction.team2 == game['opp2'],
                                                                                        Prediction.status_id == 0,
                                                                                        Prediction.type_id == 0,
                                                                                        Prediction.game_start_time > start_time,
                                                                                        Prediction.game_start_time < end_time)).all()


                                    expreses = session.query(Express).filter(and_(Express.team1 == game['opp1'],
                                                                                Express.team2 == game['opp2'],
                                                                                Express.status_id == 0,
                                                                                Express.type_id == 1,
                                                                                Express.game_start_time > start_time,
                                                                                Express.game_start_time < end_time)).all()


                                    # Удаляем текущий матч из прослушки если не осталось непроверенных матчей
                                    if event_team_online and not predictions and not expreses:
                                        session.delete(event_team_online)
                                        session.commit()
                                    





    # hours_24_ago = (current_time - hours_24)
    # time_now = datetime.utcnow() + moscow_time

    # Устанавливем временные промежутки (Время с сервера - 24 часа) в нужном формате (год:месяц:день+часы:минуты)
    # для реквеста результатов
    # date = hours_24_ago.strftime('%Y-%m-%d+%H:%M')
    # dateTo = time_now.strftime('%Y-%m-%d+%H:%M')

    # server_end_time = session.query(TimePoint).filter(and_(TimePoint.point_for == 'prediction_end',
    #                                                        TimePoint.used == True)).first()


    # Загружаем конечную дату проверки если used=1, если used=0 то dateTo остается без изменений
    # if server_end_time:
    #     dateTo = server_end_time.date.strftime('%Y-%m-%d+%H:%M')

    # Запускаем проверку доступности ресурса, если ресурс не доступен то спим 25с и пробуем снова, если ресурс доступен, то
    # загружаем результаты всех матчей за 24 часа, или 24 часа + время sleep
    # while True:
    #     result = requests.get(f'{match_results_url}date={date}&dateTo={dateTo}',
    #                         headers=match_results_headers)


    #     if result.status_code != 200:
    #         time.sleep(25)
    #         time_now += timedelta(seconds=25)
    #         dateTo = time_now.strftime('%Y-%m-%d+%H:%M')
    #         continue
    #     else:
    #         result = json.loads(result.content.decode('utf-8'))["results"]
    #         break

    # Удаляем лиги в которых не осталось матчей
    liga_events_with_teams = session.query(EventsLiga).all()
    for event in liga_events_with_teams:
        if len(event.teams) == 0:
            session.delete(event)
    session.commit()


    # Ставит точку на дату и время завершения скрипта
    server_time = session.query(TimePoint).filter(TimePoint.point_for == point_for).first()
    server_time.date = datetime.utcnow()
    session.commit()


    return f'Проверен промежуток {start_date} - {end_date}, экспрессов найдено: {express_amount} | обычных найдено: {prediction_amount}'
