# Python_______________________________
import os
import sys
import json
import requests
import time
from datetime import datetime, timedelta

# SqlAlchemy__________________________
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import and_

# Database Models_________________________
from models.timepoint import TimePoint
from models.eventsliga import EventsLiga
from models.eventsteams import EventsTeams
from models.prediction import Prediction
from models.express import Express
from models.predictionstatuses import PredictionStatuses  # Not used but needed for mapper
from models.predictiontypes import PredictionTypes  # Not used but needed for mapper
from models.likes import Like  # Not used but needed for mapper
from models.user import User
from models.statistic import Statistic
from models.views import View # Not used but needed for mapper
from models.comments import Comment # Not used but needed for mapper


# SqlAlchemy init__________________________________________________
SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
engine = create_engine(SQLALCHEMY_DATABASE_URI)
session = sessionmaker(bind=engine)()


statistics = session.query(Statistic).all()

for statistic in statistics:
    if statistic.stake_amount != 0:
        statistic.ROI = ( statistic.profit / statistic.stake_amount ) * 100
        session.commit()

