from models import Base
from sqlalchemy import Column
from sqlalchemy.orm import relationship
from sqlalchemy import Integer, String


class EventsLiga(Base):
    __tablename__ = 'eventsliga'

    id = Column(Integer, primary_key=True)
    liga = Column(String(200))
    teams = relationship("EventsTeams", backref='eventsliga')

    def __repr__(self):
        return "<Liga(id = '{0}', liga = '{1}')>".format(self.id, self.liga)