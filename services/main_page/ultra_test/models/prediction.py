from models import Base
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy import Column
from sqlalchemy import Integer, String, Float, DateTime, Boolean


class Prediction(Base): 
    __tablename__ = 'predictions'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))

    date_of_prediction = Column(DateTime)
    liga = Column(String(200))

    team1 = Column(String(120))
    team2 = Column(String(120))

    team1_img = Column(String(400))
    team2_img = Column(String(400))

    type_of_prediction = Column(String(120)) 
    coefficient = Column(Float)

    stake_amount = Column(Float)
    game_start_time = Column(DateTime)

    description = Column(String(800))
    final_stake = Column(Float)

    status_id = Column(Integer, ForeignKey('predictionstatuses.index'))
    status = relationship('PredictionStatuses')
    
    views = relationship('View')
    likes = relationship('Like')
    
    user_details = relationship('User')
    express = relationship('Express')

    type_id = Column(Integer, ForeignKey('predictiontypes.index'))
    type_ = relationship('PredictionTypes')

    comments = relationship('Comment')
    game_1x_id = Column(Integer)
    
    liga_1x_id = Column(Integer)
    


    def __repr__(self):
        return "<Prediction(id = '{0}', rating = '{1}', model = '{2}')>".format(self.id, self.user_id, self.team1)
