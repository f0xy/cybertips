from models import Base
from sqlalchemy import Column
from sqlalchemy import Integer, String


class PredictionTypes(Base):
    __tablename__ = 'predictiontypes'

    id = Column(Integer, primary_key= True)
    index = Column(Integer, unique=True)
    tag = Column(String(100))
    

    def __repr__(self):
        return "<PredictionStatuses(index = '{0}', tag = '{1}')>".format(self.index, self.tag)
