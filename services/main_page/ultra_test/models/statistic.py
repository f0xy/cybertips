from models import Base
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import ForeignKey

class Statistic(Base): 
    __tablename__ = 'statistics'

    id = Column(Integer, primary_key= True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable = False)

    month = Column(Integer, nullable = False)
    year = Column(Integer, nullable = False)

    amount_predictions = Column(Integer, nullable=False, default=0)
    profit = Column(Integer, nullable=False, default=0)

    ROI = Column(Integer,nullable=False ,default=0)
    successful_predictions = Column(Integer, nullable=False, default=0)
    
    unsuccessful_predictions = Column(Integer, nullable=False, default=0)
    stake_amount = Column(Integer, nullable=False, default=0)

    

    def __repr__(self):
        return "<Statistic(id = '{0}', user_id = '{1}', amount_predictions = '{2}')>".format(self.id, self.user_id, self.amount_predictions)