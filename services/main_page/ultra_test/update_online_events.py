# Python_______________________________
import os
import sys
import json
import requests
import time
from datetime import datetime, timedelta

# SqlAlchemy__________________________
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import and_

# Database Models_________________________
from models.timepoint import TimePoint
from models.eventsliga import EventsLiga
from models.eventsteams import EventsTeams
from models.prediction import Prediction
from models.express import Express
from models.predictionstatuses import PredictionStatuses  # Not used but needed for mapper
from models.predictiontypes import PredictionTypes  # Not used but needed for mapper
from models.likes import Like  # Not used but needed for mapper
from models.user import User
from models.statistic import Statistic
from models.views import View # Not used but needed for mapper
from models.comments import Comment # Not used but needed for mapper



# SqlAlchemy init__________________________________________________
SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
engine = create_engine(SQLALCHEMY_DATABASE_URI)
session = sessionmaker(bind=engine)()




predictions = session.query(Prediction).filter(and_(Prediction.status_id == 0,
                                                    Prediction.type_id == 0)).all()

expresses = session.query(Express).filter(and_(Express.status_id == 0,
                                               Express.type_id == 1)).all()

for prediction in predictions:
    event_game = session.query(EventsTeams).filter(EventsTeams.team1 == prediction.team1,
                                                   EventsTeams.team2 == prediction.team2,
                                                   EventsTeams.liga_id == prediction.liga_1x_id).first()

    if not event_game:
        events_liga = session.query(EventsLiga).filter(EventsLiga.id == prediction.liga_1x_id).first()

        if not events_liga:
            new_liga = EventsLiga(id = prediction.liga_1x_id, liga = prediction.liga)
            new_game = EventsTeams(liga_id = prediction.liga_1x_id, team1 = prediction.team1, team2 = prediction.team2)

            session.add(new_liga)
            session.add(new_game)

            session.commit()
            print(f'Добавлена лига {new_liga.liga} и игра в ней {prediction.team1} - {prediction.team2}')
        else:
            new_game = EventsTeams(liga_id = events_liga.id, team1 = prediction.team1, team2 = prediction.team2)
            session.add(new_game)
            session.commit()

            print(f'Добавлена игра {prediction.team1} - {prediction.team2} в лигу {events_liga.liga}')


for prediction in expresses:
    event_game = session.query(EventsTeams).filter(EventsTeams.team1 == prediction.team1,
                                                   EventsTeams.team2 == prediction.team2,
                                                   EventsTeams.liga_id == prediction.liga_1x_id).first()

    if not event_game:
        events_liga = session.query(EventsLiga).filter(EventsLiga.id == prediction.liga_1x_id).first()

        if not events_liga:
            new_liga = EventsLiga(id = prediction.liga_1x_id, liga = prediction.liga)
            new_game = EventsTeams(liga_id = prediction.liga_1x_id, team1 = prediction.team1, team2 = prediction.team2)

            session.add(new_liga)
            session.add(new_game)

            session.commit()
            print(f'Добавлена лига {new_liga.liga} и игра в ней {prediction.team1} - {prediction.team2}')
        else:
            new_game = EventsTeams(liga_id = events_liga.id, team1 = prediction.team1, team2 = prediction.team2)
            session.add(new_game)
            session.commit()

            print(f'Добавлена игра {prediction.team1} - {prediction.team2} в лигу {events_liga.liga}')
