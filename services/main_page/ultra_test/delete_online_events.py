# Python_______________________________
import os
import sys
import json
import requests
import time
from datetime import datetime, timedelta

# SqlAlchemy__________________________
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import and_

# Database Models_________________________
from models.timepoint import TimePoint
from models.eventsliga import EventsLiga
from models.eventsteams import EventsTeams
from models.prediction import Prediction
from models.express import Express
from models.predictionstatuses import PredictionStatuses  # Not used but needed for mapper
from models.predictiontypes import PredictionTypes  # Not used but needed for mapper
from models.likes import Like  # Not used but needed for mapper
from models.user import User
from models.statistic import Statistic
from models.views import View # Not used but needed for mapper
from models.comments import Comment # Not used but needed for mapper



# SqlAlchemy init__________________________________________________
SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
engine = create_engine(SQLALCHEMY_DATABASE_URI)
session = sessionmaker(bind=engine)()


events_teams_online = session.query(EventsTeams).all()


deleted_games_counter = 0                                              
deleted_ligs_counter = 0

for event in events_teams_online:
    predictions = session.query(Prediction).filter(and_(Prediction.team1 == event.team1,
                                                        Prediction.team2 == event.team2,
                                                        Prediction.status_id == 0,
                                                        Prediction.type_id == 0,
                                                        Prediction.liga_1x_id == event.liga_id)).all()

    expreses = session.query(Express).filter(and_(Express.team1 == event.team1,
                                                  Express.team2 == event.team2,
                                                  Express.status_id == 0,
                                                  Express.type_id == 1,
                                                  Express.liga_1x_id == event.liga_id)).all()

    

    if not predictions and not expreses:
        events_liga_online = session.query(EventsLiga).filter(EventsLiga.id == event.liga_id).first()

        if events_liga_online and len(events_liga_online.teams) == 1:
            session.delete(event)
            session.delete(events_liga_online)
            session.commit()
            deleted_games_counter += 1
            deleted_ligs_counter += 1

        elif events_liga_online and len(events_liga_online.teams) == 0:
            session.delete(events_liga_online)
            session.commit()
            deleted_ligs_counter += 1

        elif events_liga_online and len(events_liga_online.teams) > 1:
            session.delete(event)
            session.commit()
            deleted_games_counter += 1

print(f'Удалено лиг: {deleted_ligs_counter} Удалено игр: {deleted_games_counter}', file=sys.stdout)
