import json
import requests
import re
from datetime import datetime
from flask import render_template, url_for, redirect, request, jsonify, flash, current_app
from flask_login import LoginManager, login_user, current_user, login_required, logout_user
from werkzeug.security import check_password_hash, generate_password_hash
from itsdangerous import URLSafeTimedSerializer, SignatureExpired
from flask_mail import Mail, Message
from app.blueprints.auth import auth
from app import mail
from app import executor


from app.database import db
from app.database.models.user import User
from app.database.models.notification import Notification
from app.database.models.statistic import Statistic


serializer = URLSafeTimedSerializer('Thisisabigsecret!')

def send_email(email, email_title, title, message, btn_text, link, html, salt, nickname=None):
    token = serializer.dumps(email, salt=salt)
    msg = Message(email_title, sender=current_app.config['MAIL_USERNAME'], recipients=[email])
    link = url_for(link, token=token, _external=True)
    msg.html = render_template(html, title=title, message=message, link=link, btn_text=btn_text, nickname=None)
    mail.send(msg)

    

@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main_page.index'))

    email = request.form.get('email')
    password = request.form.get('password')

    if email and password:

        user = User.query.filter(User.email == email).first()

        if user and check_password_hash(user.password, password):
            if user.confirmed:
                login_user(user, remember=request.form.get('remember'))
                return redirect(url_for('main_page.index'))
            else:
                return render_template("confirmed.html")
            return redirect(url_for('main_page.index'))

        flash('Неправильный логин или пароль')
        return render_template('login.html')
    
    return render_template('login.html')


@auth.route('/register', methods=['GET', 'POST'])
def register():

    if current_user.is_authenticated:
        return redirect(url_for('main_page.index'))

    nickname = request.form.get('nickname')
    email = request.form.get('email')
    password1 = request.form.get('password1')
    password2 = request.form.get('password2')

    if nickname and email and password1 and password2:
        
        if not password1 == password2:
            flash('Пароли не совпадают')
            return render_template('register.html')

        check_nickname = User.query.filter(User.nickname == nickname).first()
        check_email = User.query.filter(User.email == email).first()

        if check_nickname:
            flash('Пользователь с таким никнеймом уже существует')
            return render_template('register.html')

        if check_email:
            flash('Пользователь с таким email уже существует')
            return render_template('register.html')

        
        user = User (
            nickname = nickname,
            password = generate_password_hash(password1),
            email = email,
            phone = None,
            balance = 2000,
            date_of_registration = datetime.utcnow(),
            rating = 0.0,
            confirmed = False
        )
        
        db.session.add(user)
        db.session.flush()
        db.session.commit()

        if user:
            message = "Вы были зарегистрированы на Cybertips.net. Нажмите на кнопку, чтобы подтвердить регистрацию"

            executor.submit(send_email, email, 'Cybertips - Подтверждение регистрации', 'Подтверждение регистрации', message,
            'Подтвердить email', 'auth.confirm_email', 'email_template.html', 'email-confirm', user.nickname)

            return render_template('confirmed.html', email=email)

        flash('Ошибка' + str(user))
        return render_template('register.html')
        
    return render_template('register.html')

@auth.route('/password_change', methods=['GET', 'POST'])
def email_changing_password():
    if current_user.is_authenticated:
        return redirect(url_for('main_page.index'))

    return render_template("email_changing_password.html")

@auth.route('/recover_password', methods=['GET', 'POST'])
def recover_password():
    email = request.form.get('email')
    if current_user.is_authenticated:
        return redirect(url_for('main_page.index'))
    if email:
        user = User.query.filter(User.email == email).first()
        if user:
                message = "Нажмите на кнопку, чтобы подтвердить смену пароля"

                executor.submit(send_email, email, 'Cybertips - восстановление пароля', 'Восстановление пароля', message,
                'Подтвердить смену пароля', 'auth.recover_password_link', 'email_template.html', 'password-recover', user.nickname)

                return render_template('recover_password.html', email=email)
        else:
            return render_template('error_changing_password.html', email=email)


@auth.route('/setup_settings')
def new_password_user():
    email = request.args.get('email')
    if email:
        user = User.query.filter(User.email == email).first()
        if user.recover_pass == False:
            return url_for('auth.email_changing_password')
        else:
            return render_template('new_password_user.html', email=email)
    else:
        return 'no email'


@auth.route('/submit_password_user', methods=['GET', 'POST'])
def submit_password_user():
    email = request.args.get('email')
    password1 = request.form.get('password1')
    password2 = request.form.get('password2')
    if email:
        if not password1 == password2:
            flash('Пароли не совпадают')
            return redirect(url_for('auth.new_password_user', email=email))
        user = User.query.filter(User.email == email).first()
        if user.recover_pass == False:
            return url_for('auth.email_changing_password')
        else:
            user.password = generate_password_hash(password2)
            user.recover_pass = False
            db.session.commit()
            return redirect(url_for('auth.login'))

@auth.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    if not current_user.is_authenticated:
        return redirect(url_for('auth.login'))
    logout_user()
    return redirect(url_for('main_page.index'))


@auth.route('/confirm/<token>')
def confirm_email(token):
    try:
        email = serializer.loads(token, salt="email-confirm", max_age=43200)
        
        user = User.query.filter(User.email == email).first()

        if user.confirmed == True:
            flash('Аккаунт уже подтверждён, пожалуйста залогиньтесь.', 'success')
            return redirect(url_for('auth.login'))
        else:
            user.confirmed = True
            user.confirmed_on = datetime.utcnow()
            db.session.commit()

            notification = Notification (
                user_id = user.id,
                date = datetime.utcnow(),
                description = 'Вы успешно зарегистрированы.'
            )

            db.session.add(notification)

            statistic = Statistic (
                user_id = user.id,
                month = datetime.utcnow().month,
                year = datetime.utcnow().year
            )

            db.session.add(statistic)

            db.session.commit()

            login_user(user)

        return redirect(url_for('main_page.index'))
    except SignatureExpired:
        flash('The confirmation link is invalid or has expired.', 'danger')

@auth.route('/recover_password/<token>')
def recover_password_link(token):
    try:
        email = serializer.loads(token, salt="password-recover", max_age=43200)
        user = User.query.filter(User.email == email).first()
        if user:
            if user.recover_pass == True:
                flash('Password recover already confirmed. Please change it.', 'success')
                return redirect(url_for('auth.new_password_user', email=email))
            else:
                user.recover_pass = True
                db.session.commit()
                return redirect(url_for('auth.new_password_user', email=email))
    except SignatureExpired:
        flash('The confirmation link is invalid or has expired.', 'danger')
