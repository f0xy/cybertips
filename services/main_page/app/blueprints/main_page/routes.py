import os
import sys
import traceback
from functools import reduce
from app.config import Config
import requests, json, re, time
import eventlet
requests = eventlet.import_patched("requests") #NOT DELETE WITHOUT ANY Q
from datetime import datetime, timedelta
from flask import render_template, url_for, redirect, request, flash, jsonify
from flask_login import LoginManager, login_user, current_user, login_required, logout_user
from app.blueprints.main_page import main_page
from werkzeug.security import check_password_hash, generate_password_hash

from itertools import groupby # при добавлении прогноза группирует экспрессы
from app import cache
# Database Models___________________________________________________
from app.database import db
from app.database.models.user import User
from app.database.models.admin import Admin
from app.database.models.prediction import Prediction
from app.database.models.predictionstatuses import PredictionStatuses
from app.database.models.predictiontypes import PredictionTypes
from app.database.models.comment import Comment
from app.database.models.notification import Notification
from app.database.models.timepoint import TimePoint
from app.database.models.eventsliga import EventsLiga
from app.database.models.eventsteams import EventsTeams
from app.database.models.comment import Comment
from app.database.models.subscribers import Subscribe
from app.database.models.statistic import Statistic
from app.database.models.express import Express
from app.database.models.likes import Like
from app.database.models.views import View

from sqlalchemy import and_, or_


# Main Variables_________________________________________
team_logo_path = 'https://1xstavka.ru/sfiles/logo_teams/'
vk_link_path = 'https://vk.com/id'


@cache.cached(timeout=300, key_prefix='all_dota_games')
def get_games_events():
    while True:
        try:
            dota_games_req = requests.get('https://part.upnp.xyz/PartLine/GetKiberGames?link=http://refpazyl.xyz//L?tag=d_46491m_1599c_&lng=en&filter=DOTA')
            dota_games = json.loads(dota_games_req.content.decode('utf-8'))
            for elem in dota_games:
                game_uri = elem['uri']
                event_id = re.findall(r'\d{9}', f'{game_uri}')
                event = json.loads(requests.get(f'https://1xstavka.ru/LineFeed/GetGameZip?id={event_id[0]}&lng=ru').content.decode('utf-8'))
                if event:
                    if len(event['Value']['E']) == 0 or not event['Value']['E'] or not elem['team1odds'] or not elem['team2odds'] or not elem['liga'] or not elem['team1'] or not elem['team2']:
                        dota_games.remove(elem)
            return dota_games
        except:
            time.sleep(1)
            continue

@main_page.route('/', methods=['GET', 'POST'])
def index():
    
    choosen_team1 = request.args.get('choosen_team1')
    choosen_team2 = request.args.get('choosen_team2')
    choosen_leagues = request.args.get('choosen_leagues')
    type_of_predictions = request.args.get('type_pred')

    page = request.args.get('page', 1, type=int)
    per_page = 10

    predictions = []

    # # dota_games_req = requests.get('https://part.upnp.xyz/PartLine/GetKiberGames?link=http://refpazyl.xyz//L?tag=d_46491m_1599c_&lng=en&filter=DOTA')
    # # if dota_games_req.status_code != 200:
    # #     return redirect(url_for("main_page.index"))
    # # dota_games = json.loads(dota_games_req.content.decode('utf-8'))
    #
    dota_games = get_games_events()

    # # game_last_time = min([datetime.fromtimestamp(int(x['start'][6:-2])/1000).strftime('%Y-%m-%d %H:%M:%S.%f') for x in dota_games])


    if choosen_leagues:
        choosen_leagues = choosen_leagues.split(",")
    choosen_teams = []
    if choosen_team1 and choosen_team2:
        choosen_teams = [choosen_team1, choosen_team2]
    if not choosen_team1 and choosen_team2:
        choosen_team1 = choosen_team2
        choosen_team2 = None


    # скрипт поиска прогнозов
    expresses = []
    if choosen_leagues or choosen_team1 or choosen_team2:
        type_of_predictions = 'search'
        if not choosen_team1 and not choosen_team2:
            expresses = Express.query.filter( Express.liga.in_((choosen_leagues)) )\
                .filter( Express.status_id == 0 ).all()
            express_ids = []
            if expresses:
                express_ids = [ express.prediction_id for express in expresses ]
            predictions = Prediction.query.filter( or_(and_(Prediction.liga.in_((choosen_leagues)), Prediction.status_id == 0), Prediction.id.in_((express_ids)) ) ) \
                .order_by(Prediction.date_of_prediction.desc())\
                .paginate(page,per_page,error_out=False)

        elif not choosen_team2:
            if not choosen_leagues:
                expresses = Express.query.filter( or_(Express.team1 == choosen_team1, Express.team2 == choosen_team1 ) )\
                    .filter( Express.status_id == 0 ).all()
                express_ids = []
                if expresses:
                    express_ids = [ express.prediction_id for express in expresses ]
                predictions = Prediction.query.filter( or_(and_(or_(Prediction.team1 == choosen_team1, Prediction.team2 == choosen_team1), Prediction.status_id == 0), Prediction.id.in_((express_ids)) ) )\
                    .order_by(Prediction.date_of_prediction.desc())\
                    .paginate(page,per_page,error_out=False)

            else:
                expresses = Express.query.filter( or_(Express.team1 == choosen_team1, Express.team2 == choosen_team1 ) )\
                    .filter( Express.liga.in_((choosen_leagues)) )\
                    .filter( Express.status_id == 0 ).all()
                express_ids = []
                if expresses:
                    express_ids = [ express.prediction_id for express in expresses ]
                predictions = Prediction.query.filter( or_(and_(or_(Prediction.team1 == choosen_team1, Prediction.team2 == choosen_team1 ), Prediction.liga.in_((choosen_leagues)), Prediction.status_id == 0), Prediction.id.in_((express_ids)) ) )\
                    .order_by(Prediction.date_of_prediction.desc())\
                    .paginate(page,per_page,error_out=False)

        else:
            if not choosen_leagues:
                expresses = Express.query.filter( and_(Express.team1.in_((choosen_teams)), Express.team2.in_((choosen_teams)) ) )\
                    .filter( Express.status_id == 0 ).all()
                express_ids = []
                if expresses:
                    express_ids = [ express.prediction_id for express in expresses ]
                predictions = Prediction.query.filter( or_(and_(and_(Prediction.team1.in_((choosen_teams)), Prediction.team2.in_((choosen_teams)) ), Prediction.status_id == 0), Prediction.id.in_((express_ids)) ) )\
                    .order_by(Prediction.date_of_prediction.desc())\
                    .paginate(page,per_page,error_out=False)

                if predictions.total == 0:
                    expresses = Express.query.filter( or_(Express.team1.in_((choosen_teams)), Express.team2.in_((choosen_teams)) ) )\
                        .filter( Express.status_id == 0 ).all()
                    express_ids = []
                    if expresses:
                        express_ids = [ express.prediction_id for express in expresses ]
                    predictions = Prediction.query.filter( or_(and_(or_(Prediction.team1.in_((choosen_teams)), Prediction.team2.in_((choosen_teams)) ), Prediction.status_id == 0), Prediction.id.in_((express_ids)) ) )\
                        .order_by(Prediction.date_of_prediction.desc())\
                        .paginate(page,per_page,error_out=False)
            else:
                expresses = Express.query.filter( and_(Express.team1.in_((choosen_teams)), Express.team2.in_((choosen_teams)) ) )\
                    .filter( Express.liga.in_((choosen_leagues)) )\
                    .filter( Express.status_id == 0 ).all()
                express_ids = []
                if expresses:
                    express_ids = [ express.prediction_id for express in expresses ]
                predictions = Prediction.query.filter( or_(and_(and_(Prediction.team1.in_((choosen_teams)), Prediction.team2.in_((choosen_teams)) ), Prediction.liga.in_((choosen_leagues)), Prediction.status_id == 0), Prediction.id.in_((express_ids)) ) )\
                    .order_by(Prediction.date_of_prediction.desc())\
                    .paginate(page,per_page,error_out=False)

                if predictions.total == 0:
                    expresses = Express.query.filter( or_(Express.team1.in_((choosen_teams)), Express.team2.in_((choosen_teams)) ) )\
                        .filter( Express.liga.in_((choosen_leagues)) )\
                        .filter( Express.status_id == 0 ).all()
                    express_ids = []
                    if expresses:
                        express_ids = [ express.prediction_id for express in expresses ]
                    predictions = Prediction.query.filter( or_(and_(or_(Prediction.team1.in_((choosen_teams)), Prediction.team2.in_((choosen_teams)) ), Prediction.liga.in_((choosen_leagues)), Prediction.status_id == 0), Prediction.id.in_((express_ids)) ) )\
                        .order_by(Prediction.date_of_prediction.desc())\
                        .paginate(page,per_page,error_out=False)
    else:
        if type_of_predictions == "actual":
            expresses = Express.query.filter( Express.status_id == 0 ).all()
            express_ids = []
            if expresses:
                express_ids = [ express.prediction_id for express in expresses ]
            predictions = Prediction.query.filter( or_(Prediction.status_id == 0, Prediction.id.in_((express_ids)) ) ) \
                .order_by(Prediction.date_of_prediction.desc())\
                .paginate(page,per_page,error_out=False)
        elif type_of_predictions == 'all':
            predictions = Prediction.query.order_by(Prediction.date_of_prediction.desc())\
                .paginate(page,per_page,error_out=False)
        else:
            expresses = Express.query.filter( Express.status_id == 0 ).all()
            express_ids = []
            if expresses:
                express_ids = [ express.prediction_id for express in expresses ]
            predictions = Prediction.query.filter( or_(Prediction.status_id == 0, Prediction.id.in_((express_ids)) ) ) \
                .order_by(Prediction.date_of_prediction.desc())\
                .paginate(page,per_page,error_out=False)
    if not predictions:
        predictions = []


    # чтобы сохранить выбраннные матчи визуально
    choosen_leagues_list = []
    if choosen_leagues:
        choosen_leagues_list = [ league_slim[8:] for league_slim in choosen_leagues]
        choosen_leagues = ",".join(choosen_leagues)

    liked_list=[]
    if current_user.is_authenticated:
        rf = json.dumps({'filters': [{'name': 'user_id', 'op': 'eq', 'val': current_user.get_id()}]})
        liked = json.loads(requests.get(f'http://api/api/v1/likes?q={rf}&results_per_page=100000').content.decode('utf-8'))['objects']
        liked_list = [ prediction['prediction_id'] for prediction in liked ]


    dota_leagues = []
    dota_teams = []
    for game in dota_games:
        dota_leagues.append(game['liga'][8:])
        dota_teams.append(game['team1'])
        dota_teams.append(game['team2'])
    dota_leagues = sorted(list(set(dota_leagues)), key=str.lower)
    dota_teams = json.dumps(list(set(dota_teams)))

    user_ip = request.headers['X-Forwarded-For']


    return render_template('feed.html', predictions=predictions, dota_games=dota_games, logo_path=team_logo_path, liked=liked_list, dota_leagues=dota_leagues, dota_teams=dota_teams, choosen_team1=choosen_team1, choosen_team2=choosen_team2, choosen_leagues=choosen_leagues, choosen_leagues_list=choosen_leagues_list, user_ip=user_ip, type_of_predictions=type_of_predictions)

@main_page.route('/filtering_index', methods=['GET', 'POST'])
def filtering_index():

    choosen_team1 = request.form.get('team1')
    choosen_team2 = request.form.get('team2')
    choosen_leagues = request.form.get('choosen_leagues')

    return redirect(url_for('main_page.index', choosen_team1=choosen_team1, choosen_team2=choosen_team2, choosen_leagues=choosen_leagues))



@main_page.route('/paid-prognos-non-access', methods=['GET', 'POST'])
def paid_prognos_non_access():

    return render_template('paid-prognos-non-access.html')


@main_page.route('/send_comment', methods=['GET', 'POST'])
def send_comment():

    reply_id = request.form.get('reply_details') if request.form.get('reply_details') != "" else None
    prediction_id = request.args.get('prediction_id')
    
    description = request.form.get('comment')

    new_comment = {
        'user_id': current_user.get_id(),
        'date': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
        'description': description,
        'prediction_id': prediction_id,
        'reply_id': reply_id
    }
    requests.post('http://api/api/v1/comments', data=json.dumps(new_comment), headers={'content-type': 'application/json'}).content.decode('utf-8')


    return redirect(url_for('main_page.prognos_details', prediction=prediction_id))

@main_page.route('/prediction_details', methods=['GET', 'POST', 'PATCH'])
def prognos_details():

    prediction_id = request.args.get('prediction')

    if not prediction_id:
        return redirect(url_for('main_page.index'))

    prediction = requests.get(f'http://api/api/v1/predictions/{prediction_id}')

    if prediction.status_code != 200:
        bad_status_code =  prediction.status_code
        return redirect(url_for('main_page.index'))

    prediction = json.loads(prediction.content.decode('utf-8'))

    rf = json.dumps({'filters': [{'name': 'prediction_id', 'op': 'eq', 'val': prediction_id}]})

    if request.headers.getlist("X-Forwarded-For"):
        user_ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        user_ip = request.remote_addr
    is_view_add = False
    views = json.loads(requests.get(f'http://api/api/v1/views?q={rf}').content.decode('utf-8'))['objects']
    if len([i for i in views if i['user_ip'] == user_ip ]) == 0 and prediction_id:
        is_view_add = True
        new_view = {
        'user_ip': user_ip,
        'prediction_id': prediction_id
        }
        requests.post('http://api/api/v1/views', data=json.dumps(new_view), headers={'content-type': 'application/json'}).content.decode('utf-8')
    

    comments = json.loads(requests.get(f'http://api/api/v1/comments?q={rf}').content.decode('utf-8'))['objects']
    
    #SORTING COMMENTS
    id_column = {}

    for j in comments:
        if j['reply_id'] == None:
            id_column.setdefault(j['id'], [])
    
    for id_ in id_column.keys():
        for comment in comments:
            if id_ == comment['reply_id']:
                id_column[id_].append(comment['id'])

    
    
    def find_reply(current_comment, comments, id_):
        next_id_ = None
        for comment in comments:
            if comment['reply_id'] == id_:
                next_id_ = comment['id']
                current_comment['reply_comments'].append(comment)

        if next_id_:
            return find_reply(current_comment, comments, next_id_)
        else:
            return current_comment


    sorted_comments = []
    for comment in comments:
        if comment['id'] in id_column.keys():
            comment.setdefault('reply_comments', [])
            comment = find_reply(comment, comments, comment['id'])
            sorted_comments.append(comment)


    sorted_comments.reverse()

    #CURRENT_USER INFO    
    user_id = current_user.get_id()
    status = current_user.is_authenticated

    date_prognos = prediction['date_of_prediction']

    def time_parser(data):
        hours = data[11:13]
        minutes = data[14:16]
        month = data[5:7]
        year = data[0:4]

        return '{hours}:{minutes} {months}.{year}'.format(hours=hours, minutes=minutes, months=month, year=year)

    date_prognos = time_parser(date_prognos)
    # date_prognos = datetime.strptime(date_prognos,"%Y-%m-%dT%H:%M:%S")

    liked_list=[]
    if current_user.is_authenticated:
        rf = json.dumps({'filters': [{'name': 'user_id', 'op': 'eq', 'val': current_user.get_id()}]})
        liked = json.loads(requests.get(f'http://api/api/v1/likes?q={rf}').content.decode('utf-8'))['objects']
        liked_list = [ prediction['prediction_id'] for prediction in liked ]
        
    return render_template('prognos-details.html', comments=comments, prediction=prediction, num_of_comments=len(comments), sorted_comments=sorted_comments,
     status=status, user_id=user_id, date_prognos=date_prognos, logo_path=team_logo_path, liked=liked_list, user_ip=user_ip, is_view_add=is_view_add)


@main_page.route('/user_ratings', methods=['GET', 'POST'])
def rating_people():
    current_date = datetime.now()
    month = current_date.month
    year = current_date.year

    statistics = db.session.query(Statistic, User)\
        .filter(User.id==Statistic.user_id)\
        .all()
    # .filter(and_(Statistic.month == month, Statistic.year == year))
    
    statistic_pred = sorted(statistics, key=lambda statistic: statistic.Statistic.amount_predictions)
    statistic_main = sorted(statistics, key=lambda statistic: statistic.Statistic.profit)
    statistic_pred.reverse()
    statistic_main.reverse()

    users_positions = {}
    curr_user = []
    if current_user.is_authenticated:
        curr_user_buffer = [elem for elem in statistic_pred if int(elem.Statistic.user_id) == int(current_user.id) ]
        if len(curr_user_buffer) > 0:
            curr_user = curr_user_buffer[0]
    
    for i in range(len(statistic_main)):
        users_positions.setdefault(statistic_main[i].Statistic.user_id, i+1)


    return render_template('rating-people.html', statistics=statistic_pred, users_positions=users_positions, curr_user=curr_user)


@main_page.route('/maintenance', methods=['GET', 'POST'])
def maintenance():

    return render_template('maintenance.html')


@main_page.route('/account', methods=['GET', 'POST'])
def account():
    user_id = request.args.get('user')

    if not user_id:
        return redirect(url_for('main_page.index'))

    user = requests.get(f'http://api/api/v1/users/{user_id}')

    if user.status_code != 200:
        bad_status_code =  user.status_code
        return redirect(url_for('main_page.index'))

    rf = json.dumps({'filters': [
        {'name': 'user_id', 'op': 'eq', 'val': user_id},
        {'name': 'status_id', 'op': 'in', 'val': [3, 2, 0, 5]}
        ]
    })

    #apex graph
    predictions = json.loads(requests.get(f'http://api/api/v1/predictions?q={rf}&results_per_page=100000').content.decode('utf-8'))['objects']
    apex_data = []
    for prediction in predictions:
        if prediction['status_id'] in [3, 2, 5]:
            if prediction['final_stake']:
                current_date = datetime.timestamp(datetime.strptime(prediction['date_of_prediction'], '%Y-%m-%dT%H:%M:%S'))
                apex_data.append([str(int(current_date*1000)), str(int(prediction['final_stake'])) ])
    predictions = predictions[::-1]

    apex_dict = {}
    # суммируем все суммы ставок до определнной даты
    apex_data = [ [ apex_el[0], str(sum([ int(stake[1]) for stake in apex_data if int(apex_el[0]) >= int(stake[0]) ])) ] for apex_el in apex_data] 
    for i in range(len(apex_data)):
        apex_dict.setdefault(i, ', '.join(apex_data[i]))
    apex_dict = json.dumps(apex_dict)
    
    #user info
    user_rf = json.dumps({'filters': [
        {'name': 'user_id', 'op': 'eq', 'val': user_id}
        ]
    })
    
    user_info = json.loads(requests.get(f'http://api/api/v1/statistics?q={user_rf}').content.decode('utf-8'))['objects']
    user_wr_suc = 0
    user_pred = 0
    user_profit = 0
    user_stakes = 0
    for user_attr in user_info:
        user_wr_suc += int(user_attr['successful_predictions'])
        user_pred += int(user_attr['amount_predictions'])
        user_profit += int(user_attr['profit'])
        user_stakes += int(user_attr['stake_amount'])

    if user_stakes:
        user_ROI = int( ( user_profit + user_stakes - user_stakes )*100 / user_stakes )
    else:
        user_ROI = 0
    
    if user_pred:
        user_wr = int(user_wr_suc*100 / user_pred)
    else:
        user_wr = 0

    user_obj = [user_wr, user_ROI, user_pred, user_profit]


    user = json.loads(user.content.decode('utf-8'))

    #statistics
    current_date = datetime.now()
    year = current_date.year
    month = current_date.month
    this_months_in_year = [i for i in range(1, month+1)]
    next_months_in_year = []
    next_year = False
    if len(this_months_in_year) != 12:
        next_months_in_year = [i for i in range(month, 13)]
        next_year = year - 1

    rf = json.dumps({'filters': [
        {'name': 'user_id', 'op': 'eq', 'val': user_id},
        {'name': 'month', 'op': 'in', 'val': this_months_in_year},
        {'name': 'year', 'op': 'eq', 'val': year}
        ]
    })
    this_year_stat = json.loads(requests.get(f'http://api/api/v1/statistics?q={rf}').content.decode('utf-8'))['objects']

    next_year_stat = False
    if next_year:
        rf_1 = json.dumps({'filters': [
            {'name': 'user_id', 'op': 'eq', 'val': user_id},
            {'name': 'month', 'op': 'in', 'val': next_months_in_year},
            {'name': 'year', 'op': 'eq', 'val': next_year}
        ]
        })
        next_year_stat = json.loads(requests.get(f'http://api/api/v1/statistics?q={rf_1}').content.decode('utf-8'))['objects']
    statistics = []
    try:
        statistics = this_year_stat + next_year_stat
    except:
        pass
    months_default = {
        1: 'Янв',
        2: 'Фев',
        3: 'Март',
        4: 'Апр',
        5: 'Май',
        6: 'Июнь',
        7: 'Июль',
        8: 'Авг',
        9: 'Сент',
        10: 'Окт',
        11: 'Нояб',
        12: 'Дек'
        }
    for statistic in statistics:
        statistic['month'] = months_default[statistic['month']]


    rf = json.dumps({'filters': [
        {'name': 'subscription_id', 'op': 'eq', 'val': user_id}]
    })
    subscriptions = json.loads(requests.get(f'http://api/api/v1/subscribers?q={rf}').content.decode('utf-8'))

    rf = json.dumps({'filters': [
        {'name': 'subscriber_id', 'op': 'eq', 'val': user_id}]
    })
    subscribers = json.loads(requests.get(f'http://api/api/v1/subscribers?q={rf}').content.decode('utf-8'))


    count_subscribers = subscriptions['num_results']
    count_subscriptions = subscribers['num_results']
    count_subs = [count_subscribers, count_subscriptions]

    subscriptions = subscriptions['objects']
    is_subscriber = False
    if current_user.is_authenticated:
        our_id = current_user.get_id()
        for elem in subscriptions:
            if int(our_id) == int(elem['subscriber_id']):
                is_subscriber = True
    
    predictions = predictions[:20]


    return render_template('account-page.html', user=user, predictions=predictions, apexdata=apex_dict, statistics=statistics, logo_path=team_logo_path, user_obj=user_obj, vk_link_path=vk_link_path, is_subscriber=is_subscriber, count_subs=count_subs)

@main_page.route('/faq', methods=['GET', 'POST'])
def faq():

    return render_template('FAQ.html')

@main_page.route('/add_tip', methods=['GET', 'POST'])
@login_required
def add_prognos():
    # dota_games_req = requests.get('https://part.upnp.xyz/PartLine/GetKiberGames?link=http://refpazyl.xyz//L?tag=d_46491m_1599c_&lng=en&filter=DOTA')
    # if dota_games_req.status_code != 200:
    #     return redirect(url_for("main_page.add_prognos"))
    # dota_games = json.loads(dota_games_req.content.decode('utf-8'))
    # for elem in dota_games:
    
    #     game_uri = elem['uri']
    #     event_id = re.findall(r'\d{8}', f'{game_uri}')
    #     try:
    #         event = json.loads(requests.get(f'https://1xstavka.ru/LineFeed/GetGameZip?id={event_id[0]}&lng=ru').content.decode('utf-8'))
    #     except:
    #         return redirect(url_for('main_page.index'))
    #     if event:
    #         if len(event['Value']['E']) == 0 or not event['Value']['E'] or not elem['team1odds'] or not elem['team2odds'] or not elem['liga'] or not elem['team1'] or not elem['team2']:
    #             dota_games.remove(elem)

    dota_games = get_games_events()


    if dota_games:
        for game in dota_games:
            if 'Winner' in game['liga']:
                dota_games.remove(game)
    
    user_rf = json.dumps({'filters': [{'name': 'user_id', 'op': 'eq', 'val': current_user.get_id()}]})
    user_predictions = json.loads(requests.get(f'http://api/api/v1/predictions?q={user_rf}&results_per_page=100000').content.decode('utf-8'))['objects']
    # test1 = []
    # if user_predictions:
    #     for curr_elem in dota_games: #все матчи с 1х
    #         for user_elem in user_predictions: # все матчи на которые мы ставили
    #             if str(user_elem['game_1x_id']) in curr_elem['uri'] and str(user_elem['liga_1x_id']) in curr_elem['uri']:
    #                 test1.append([user_elem, curr_elem])
    #                 dota_games.remove(curr_elem)

                    

    dota_leagues = []
    for game in dota_games:
        dota_leagues.append(game['liga'][8:])
    dota_leagues = list(set(dota_leagues))

    dota_games_dumps = json.dumps(dota_games).replace("'", "`")

    return render_template('add-prognos.html', all_leagues_dota=dota_leagues, dota_games=dota_games_dumps, logo_path=team_logo_path, user_predictions=user_predictions, testtest=dota_games)


@main_page.route('/user_predictions', methods=['GET', 'POST'])
def user_predictions():

    user_id = request.args.get('user')
    type_of_predictions = request.args.get('type_pred')

    user = requests.get(f'http://api/api/v1/users/{user_id}')

    if user.status_code != 200:
        bad_status_code =  user.status_code
        return redirect(url_for('main_page.index'))
    else:
        user = json.loads(user.content.decode('utf-8'))

    page = request.args.get('page', 1, type=int)
    per_page = 10

    if type_of_predictions == "actual":
        expresses = Express.query.filter( and_(Express.status_id == 0, Express.user_id == user_id)).all()
        express_ids = []
        if expresses:
            express_ids = [ express.prediction_id for express in expresses ]
        predictions = Prediction.query.filter(Prediction.user_id == user_id)\
            .filter( or_(Prediction.status_id == 0, Prediction.id.in_((express_ids)) ) ) \
            .order_by(Prediction.date_of_prediction.desc())\
            .paginate(page,per_page,error_out=False)

    elif type_of_predictions == "all":
        expresses = Express.query.filter(Express.user_id == user_id).all()
        express_ids = []
        if expresses:
            express_ids = [ express.prediction_id for express in expresses ]
        predictions = Prediction.query.filter( or_(Prediction.id.in_((express_ids)), Prediction.user_id == user_id )) \
            .order_by(Prediction.date_of_prediction.desc())\
            .paginate(page,per_page,error_out=False)
    
    else:
        predictions = []

    liked_list=[]
    if current_user.is_authenticated:
        rf = json.dumps({'filters': [{'name': 'user_id', 'op': 'eq', 'val': current_user.get_id()}]})
        liked = json.loads(requests.get(f'http://api/api/v1/likes?q={rf}&results_per_page=100000').content.decode('utf-8'))['objects']
        liked_list = [ prediction['prediction_id'] for prediction in liked ]
    
    user_ip = request.headers['X-Forwarded-For']


    return render_template('user_predictions.html', predictions=predictions, logo_path=team_logo_path, liked_list=liked_list, user=user, type_of_predictions=type_of_predictions)


@main_page.route('/user_settings', methods=['GET', 'POST', 'PATCH'])
@login_required
def update_profile():

    # user = {
    #     'id': current_user.id,
    #     'email': current_user.email,

    #     'password': current_user.password,
    #     'nickname': current_user.nickname,

    #     'phone': current_user.phone,
    #     'date_of_registration': current_user.date_of_registration,

    #     'balance': current_user.balance,
    #     'rating': current_user.rating,

    #     'total_earn': current_user.total_earn,
    #     'amount_predictions': current_user.amount_predictions,

    #     'profit': current_user.profit,
    #     'ROI': current_user.ROI,

    #     'successful_predictions': current_user.successful_predictions,
    #     'unsuccessful_predictions': current_user.unsuccessful_predictions,
        
    #     'name': current_user.name,
    #     'second_name': current_user.second_name,

    #     'description': current_user.description,
    #     'confirmed': current_user.confirmed,

    #     'confirmed_on': current_user.confirmed_on,
    #     'link_vk': current_user.link_vk,

    #     'link_fb': current_user.link_fb,
    #     'link_twitter': current_user.link_twitter,
        
    #     'recover_pass': current_user.recover_pass,
    #     'user_img': current_user.user_img
    # }

    # if not user['description']:
    #     user['description'] = ''

    user_rf = json.dumps({'filters': [
        {'name': 'user_id', 'op': 'eq', 'val': current_user.id}
        ]
    })
    
    user_info = json.loads(requests.get(f'http://api/api/v1/statistics?q={user_rf}').content.decode('utf-8'))['objects']
    user_wr_suc = 0
    user_pred = 0
    user_profit = 0
    user_stakes = 0

    for user_attr in user_info:
        user_wr_suc += int(user_attr['successful_predictions'])
        user_pred += int(user_attr['amount_predictions'])
        user_profit += int(user_attr['profit'])
        user_stakes += int(user_attr['stake_amount'])

    if user_stakes:
        user_ROI = int( ( user_profit + user_stakes - user_stakes )*100 / user_stakes )
    else:
        user_ROI = 0
    
    if user_pred:
        user_wr = int(user_wr_suc*100 / user_pred)
    else:
        user_wr = 0

    user_obj = [user_wr, user_ROI, user_pred, user_profit]

    user_id = current_user.get_id()
    main_user = json.loads(requests.get(f'http://api/api/v1/users/{user_id}').content.decode('utf-8'))

    return render_template('update-profile.html', user=main_user, user_obj=user_obj)

@main_page.route('/subscriptions', methods=['GET', 'POST'])
def subscriptions():

    user_id = request.args.get('user')

    if not user_id:
        return redirect(url_for('main_page.index'))

    user = requests.get(f'http://api/api/v1/users/{user_id}')
    user = json.loads(user.content.decode('utf-8'))

    user_rf = json.dumps({'filters': [
        {'name': 'user_id', 'op': 'eq', 'val': user['id']}
        ]
    })
    
    user_info = json.loads(requests.get(f'http://api/api/v1/statistics?q={user_rf}').content.decode('utf-8'))['objects']
    user_wr_suc = 0
    user_pred = 0
    user_profit = 0
    user_stakes = 0
    for user_attr in user_info:
        user_wr_suc += int(user_attr['successful_predictions'])
        user_pred += int(user_attr['amount_predictions'])
        user_profit += int(user_attr['profit'])
        user_stakes += int(user_attr['stake_amount'])

    if user_stakes:
        user_ROI = int( ( user_profit + user_stakes - user_stakes )*100 / user_stakes )
    else:
        user_ROI = 0
    
    if user_pred:
        user_wr = int(user_wr_suc*100 / user_pred)
    else:
        user_wr = 0

    user_obj = [user_wr, user_ROI, user_pred, user_profit]

    rf = json.dumps({'filters': [
        {'name': 'subscriber_id', 'op': 'eq', 'val': user_id}]
    })
    subs = json.loads(requests.get(f'http://api/api/v1/subscribers?q={rf}').content.decode('utf-8'))['objects']

    subscriptions_ids = [subscription['subscription_id'] for subscription in subs]

    subscriptions = []
    for subscription in subscriptions_ids:
        rf = json.dumps({'filters': [
        {'name': 'id', 'op': 'eq', 'val': subscription}]
        })
        subscriptions.append(requests.get(f'http://api/api/v1/users?q={rf}').content.decode('utf-8'))
    subscriptions = [json.loads(sub)['objects'] for sub in subscriptions]

    count_subs = len(subscriptions)

    return render_template('subscriptions.html', user=user, user_obj=user_obj, subscriptions=subscriptions, count_subs=count_subs)

@main_page.route('/subscribers', methods=['GET', 'POST'])
def subscribers():

    user_id = request.args.get('user')

    if not user_id:
        return redirect(url_for('main_page.index'))

    user = requests.get(f'http://api/api/v1/users/{user_id}')
    user = json.loads(user.content.decode('utf-8'))

    user_rf = json.dumps({'filters': [
        {'name': 'user_id', 'op': 'eq', 'val': user['id']}
        ]
    })
    
    user_info = json.loads(requests.get(f'http://api/api/v1/statistics?q={user_rf}').content.decode('utf-8'))['objects']
    user_wr_suc = 0
    user_pred = 0
    user_profit = 0
    user_stakes = 0
    for user_attr in user_info:
        user_wr_suc += int(user_attr['successful_predictions'])
        user_pred += int(user_attr['amount_predictions'])
        user_profit += int(user_attr['profit'])
        user_stakes += int(user_attr['stake_amount'])

    if user_stakes:
        user_ROI = int( ( user_profit + user_stakes - user_stakes )*100 / user_stakes )
    else:
        user_ROI = 0
    
    if user_pred:
        user_wr = int(user_wr_suc*100 / user_pred)
    else:
        user_wr = 0

    user_obj = [user_wr, user_ROI, user_pred, user_profit]

    rf = json.dumps({'filters': [
        {'name': 'subscription_id', 'op': 'eq', 'val': user_id}]
    })
    subs = json.loads(requests.get(f'http://api/api/v1/subscribers?q={rf}').content.decode('utf-8'))['objects']

    subscriber_ids = [subscriber['subscriber_id'] for subscriber in subs]

    subscribers = []
    for subscriber in subscriber_ids:
        rf = json.dumps({'filters': [
        {'name': 'id', 'op': 'eq', 'val': subscriber}]
        })
        subscribers.append(requests.get(f'http://api/api/v1/users?q={rf}').content.decode('utf-8'))
    subscribers = [json.loads(sub)['objects'] for sub in subscribers]
    count_subs = len(subscribers)

    return render_template('subscribers.html', user=user, user_obj=user_obj, subscribers=subscribers, count_subs=count_subs)


@main_page.route('/update_profile_submit', methods=['GET', 'POST', 'PATCH'])
@login_required
def update_profile_submit():

    email = request.form.get('update-email')
    password1 = request.form.get('password1')
    password2 = request.form.get('password2')
    name = request.form.get("update-name")
    fam = request.form.get("update-fam")
    password = request.form.get('current-password')
    info_about = request.form.get('info-about')

    if password != '' and password1 != "" and password2 != "" and password != None and password1 != None and password2 != None:
        if not check_password_hash(current_user.password, password):
            flash('Неверный текущий пароль. Пароли не совпадают!')
            return redirect(url_for('main_page.update_profile'))
        elif not password1 == password2:
            flash('Новые пароли не совпадают!')
            return redirect(url_for('main_page.update_profile'))
        elif check_password_hash(current_user.password, password):
            flash('Пароль сменён успешно')
            current_user.password = generate_password_hash(password1)
            db.session.commit()
            
    if name != current_user.name or fam != current_user.second_name or info_about != current_user.description:
        current_user.name = name
        current_user.second_name = fam
        current_user.description = info_about
        db.session.commit()
        
    return redirect(url_for('main_page.update_profile'))


@main_page.route('/upload_user_avatar', methods=['GET', 'POST'])
@login_required
def upload_user_avatar():
    allowed_content_type = ['image/png', 'image/jpg', 'image/jpeg']
    user_avatar = request.files['avatar']
    extention = None
    if user_avatar and user_avatar.content_type in allowed_content_type:
        extention = user_avatar.content_type.split('/')[-1]
    else:
        # Cообщение об ошибки
        # return render_template('test.html', exception='Недопустимое расширение файла')
        return 'False'
        
    user_id = current_user.get_id()
    user_avatar.save(os.path.join(Config.USERS_AVATAR_FOLDER, f'{user_id}.{extention}'))

    user_avatar = {'user_img': str(user_id) + '.' + str(extention)}
    requests.patch(f'http://api/api/v1/users/{user_id}', data=json.dumps(user_avatar), headers={'content-type': 'application/json'}).content.decode('utf-8')

    return 'True'


@main_page.route('/auth_vk', methods=['GET', 'POST'])
@login_required
def auth_vk():
    version = 5.124
    domain = f"https://cybertips.net/auth_vk_code"
    id_app = 7719708
    response = redirect(f"https://oauth.vk.com/authorize?client_id={id_app}&display=page&redirect_uri={domain}&response_type=code&callback&v={version}")
    
    return response

@main_page.route('/auth_vk_code', methods=['GET', 'POST'])
@login_required
def auth_vk_code():
    domain = f"https://cybertips.net/auth_vk_code"
    id_app = 7719708
    secret = "RW58ZC7HyW6Y8lsUrMIt"

    code = request.args.get('code')
    if code:
        user = current_user.get_id()
        responses = json.loads(requests.get(f"https://oauth.vk.com/access_token?client_id={id_app}&client_secret={secret}&redirect_uri={domain}&code={code}").content.decode('utf-8'))
        vk_user_id = {'link_vk': responses['user_id']}
        requests.patch(f'http://api/api/v1/users/{user}', data=json.dumps(vk_user_id), headers={'content-type': 'application/json'}).content.decode('utf-8')

        return redirect(url_for("main_page.update_profile"))
    
@main_page.route('/logout_vk', methods=['GET', 'POST'])
@login_required
def logout_vk():
    user = current_user.get_id()
    vk_user_id = {'link_vk': None}
    requests.patch(f'http://api/api/v1/users/{user}', data=json.dumps(vk_user_id), headers={'content-type': 'application/json'}).content.decode('utf-8')

    return redirect(url_for("main_page.update_profile"))


@main_page.route('/post_prediction', methods=['GET', 'POST', 'PATCH'])
@login_required
def post_prediction():
    description = request.form.get('description')
    stake_amount = float(request.form.get('stake_amount'))
    
    data = json.loads(request.form.get('data'))

    games_id = [game['game_id'] for game in data]
    types_of_predictions = [game['event_type'] for game in data]
    coefficients = [game['event_content'] for game in data]

    games_jsons = []

    last_user_prediction = Prediction.query.filter(Prediction.user_id == current_user.id).\
            order_by(Prediction.date_of_prediction.desc()).first()
    if last_user_prediction:
        left_min = (datetime.utcnow() - last_user_prediction.date_of_prediction)
        if left_min.total_seconds() < 60:
            return {"game_id": None, "status": "error", "title": "Time error!", "message": f'Нельзя ставить чаще, чем раз в 1 минуту. Осталось: {round(60 - left_min.total_seconds())} секунд.'}

    for game_id in games_id:
        game = json.loads(requests.get(f'https://1xstavka.ru/LineFeed/GetGameZip?id={game_id}&lng=ru').content.decode('utf-8'))
        games_jsons.append(game)
    
    if len(games_jsons) == 1:

        if (datetime.fromtimestamp(int(games_jsons[0]['Value']['S'])) - datetime.utcnow()).total_seconds() <= 900:
            return {"game_id": None, "status": "error", "title": "Time error!", "message": "До матча осталось меньше 15 минут."}

        user_games = Prediction.query.filter(and_(Prediction.user_id == current_user.id, Prediction.type_id == 0)).all()
        if len(user_games) != 0:
            for user_game in user_games:
                if int(games_jsons[0]['Value']['CI'])  == int(user_game.game_1x_id) and int(games_jsons[0]['Value']['LI'])  == int(user_game.liga_1x_id):
                    return {"game_id": None, "status": "error", "title": "Вы уже создавали такой прогноз!", "message": "Вы уже сделали прогноз на это событие."}

        prediction = Prediction(
            user_id = current_user.get_id(),
            date_of_prediction = datetime.utcnow(),
            liga = games_jsons[0]['Value']['L'],
            status_id = 0,
            type_id = 0,
            team1 = games_jsons[0]['Value']['O1'],
            team2 = games_jsons[0]['Value']['O2'],
            team1_img = games_jsons[0]['Value']['O1IMG'],
            team2_img = games_jsons[0]['Value']['O2IMG'],
            type_of_prediction = types_of_predictions[0],
            coefficient = float(coefficients[0]),
            stake_amount = stake_amount,
            game_start_time = datetime.fromtimestamp(int(games_jsons[0]['Value']['S'])),
            description = description,
            game_1x_id = int(games_jsons[0]['Value']['CI']),
            liga_1x_id = int(games_jsons[0]['Value']['LI'])
        )

        db.session.add(prediction)
        db.session.flush()
        db.session.commit()

        liga = EventsLiga.query.filter(EventsLiga.liga == games_jsons[0]['Value']['L']).first()

        if not liga:
            liga = EventsLiga(id = int(games_jsons[0]['Value']['LI']), liga = games_jsons[0]['Value']['L'])
            db.session.add(liga)
            db.session.flush()
            teams = EventsTeams(liga_id = liga.id, team1 = games_jsons[0]['Value']['O1'], team2 = games_jsons[0]['Value']['O2'])
            db.session.add(teams)
            db.session.commit()
        else:
            teams = EventsTeams.query.filter(and_(EventsTeams.liga_id == liga.id, 
                                                  EventsTeams.team1 == games_jsons[0]['Value']['O1'],
                                                  EventsTeams.team2 == games_jsons[0]['Value']['O2'])).first()
            if not teams:
                teams = EventsTeams(liga_id = liga.id, team1 = games_jsons[0]['Value']['O1'], team2 = games_jsons[0]['Value']['O2'])
                db.session.add(teams)
                db.session.commit()

        current_user.balance -= stake_amount
        db.session.commit()
        return {"game_id": str(prediction.id), "status": "success", "title": "Прогноз успешно добавлен!", "message": "Вы будете перенаправлены на ленту прогнозов."}

    else:
        min_team1 = ""
        min_team2 = ""
        min_time = datetime.utcnow() + timedelta(days=30)
        if len(games_jsons) != 0:
            for game in games_jsons:
                if datetime.fromtimestamp(int(game['Value']['S'])) < min_time:
                    min_time = datetime.fromtimestamp(int(game['Value']['S']))
                    min_team1 = game['Value']['O1']
                    min_team2 = game['Value']['O1']

        if (min_time - datetime.utcnow()).total_seconds() <= 900 and min_team1 and min_team2:
            return {"game_id": None, "status": "error", "title": "Time error!", "message": f'До матча {min_team1} - {min_team2} осталось меньше 15 минут.'}

        user_games = Express.query.filter(Express.user_id == current_user.id).all()
        if len(user_games) != 0:
            grouped_user_games=[list(user_game[1]) for user_game in groupby(sorted(user_games, key=lambda x_id: x_id.prediction_id), key=lambda x_id: x_id.prediction_id)]
            if len(grouped_user_games) != 0:
                for user_group in grouped_user_games:
                    group_count_games = 0 #сколько раз в группе из одиннаковых экспрессов на 1 предикшн совпали игры
                    for user_game in user_group:
                        for game in games_jsons:
                            if int(game['Value']['CI'])  == int(user_game.game_1x_id) and int(game['Value']['LI'])  == int(user_game.liga_1x_id):
                                group_count_games += 1
                                if len(user_group) == len(games_jsons) == group_count_games:
                                    return {"game_id": None, "status": "error", "title": "Вы уже создавали такой прогноз!", "message": "Вы уже сделали прогноз на это событие."}
        
        expresses = []

        prediction = Prediction (
                user_id = current_user.get_id(),
                date_of_prediction = datetime.utcnow(),
                status_id = 0,
                type_id = 1,
                coefficient = round(reduce((lambda x, y: float(x) * float(y)), coefficients), 3),
                stake_amount = stake_amount,
                description = description
            )

        db.session.add(prediction)
        db.session.flush()
        db.session.commit()
        i = 0
        for game in games_jsons:
            express = Express (
                prediction_id = prediction.id,
                user_id = current_user.get_id(),
                liga = game['Value']['L'],
                status_id = 0,
                type_id = 1,
                team1 = game['Value']['O1'],
                team2 = game['Value']['O2'],
                team1_img = game['Value']['O1IMG'],
                team2_img = game['Value']['O2IMG'],
                type_of_prediction = types_of_predictions[i],
                coefficient = coefficients[i],
                game_start_time = datetime.fromtimestamp(int(game['Value']['S'])),
                game_1x_id = int(game['Value']['CI']),
                liga_1x_id = int(game['Value']['LI'])
            )

            liga = EventsLiga.query.filter(EventsLiga.liga == game['Value']['L']).first()

            if not liga:
                liga = EventsLiga(id = int(game['Value']['LI']), liga = game['Value']['L'])
                db.session.add(liga)
                db.session.flush()
                teams = EventsTeams(liga_id = liga.id, team1 = game['Value']['O1'], team2 = game['Value']['O2'])
                db.session.add(teams)
                db.session.commit()
            else:
                teams = EventsTeams.query.filter(and_(EventsTeams.liga_id == liga.id, 
                                                  EventsTeams.team1 == game['Value']['O1'],
                                                  EventsTeams.team2 == game['Value']['O2'])).first()
                if not teams:
                    teams = EventsTeams(liga_id = liga.id, team1 = game['Value']['O1'], team2 = game['Value']['O2'])
                    db.session.add(teams)
                    db.session.commit()

            expresses.append(express)
            i += 1

        db.session.add_all(expresses)
        current_user.balance -= stake_amount
        db.session.commit()
        return {"game_id": str(prediction.id), "status": "success", "title": "Прогноз успешно добавлен!", "message": "Вы будете перенаправлены на ленту прогнозов."}
        
    return render_template('test.html')

@main_page.route('/user_agreements', methods=['GET', 'POST'])
def user_agreements():

    return render_template("user_agreements.html")