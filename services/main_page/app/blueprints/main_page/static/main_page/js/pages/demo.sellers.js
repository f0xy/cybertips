$(document).ready(function () {
    "use strict";
    var l = {
            chart: {
                type: "line",
                width: 80,
                height: 35,
                sparkline: {
                    enabled: !0
                }
            },
            series: [],
            stroke: {
                width: 2,
                curve: "smooth"
            },
            markers: {
                size: 0
            },
            colors: ["#727cf5"],
            tooltip: {
                fixed: {
                    enabled: !1
                },
                x: {
                    show: !1
                },
                y: {
                    title: {
                        formatter: function (e) {
                            return ""
                        }
                    }
                },
                marker: {
                    show: !1
                }
            }
        },
        s = [];
    var table = $("#products-datatable").DataTable({
        
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>"
            },
            info: "Показываются _START_-_END_ из _TOTAL_",
            lengthMenu: 'Показывать <select class=\'custom-select custom-select-sm ml-1 mr-1\'><option value="10">10</option><option value="20">20</option><option value="-1">Все</option></select>'
        },
        pageLength: 10,
        
        "aoColumns": [
            { "orderSequence": [ "asc", "desc" ] },
            { "orderSequence": [ "desc", "asc" ] },
            { "orderSequence": [ "desc", "asc" ] },
            { "orderSequence": [ "asc", "desc" ], "orderData" : 0 },
            { "orderSequence": [ "desc", "asc" ] },
            { "orderSequence": [ "desc", "asc" ] }
        ],

        select: {
            style: "multi"
        },
        order: [
            [0, "asc"]
        ],
        drawCallback: function () {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded");
            for (var e = 0; e < s.length; e++) try {
                s[e].destroy()
            } catch (e) {
                console.log(e)
            }
            s = [], $(".spark-chart").each(function (e) {
                var t = $(this).data().dataset;
                l.series = [{
                    data: t
                }];
                var o = new ApexCharts($(this)[0], l);
                s.push(o), o.render()
            })
        },
    })
    $("input").on('keyup', function () {
        table
            .columns( 1 )
            .search( this.value )
            .draw();
    });
});