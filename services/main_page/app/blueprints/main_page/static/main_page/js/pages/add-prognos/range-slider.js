// range slider js
var $range = $(".js-range-slider"),
$input = $("#js-range-slider-changing"),
instance

$range.ionRangeSlider({
    type: "single",
    min: 0,
    max: 10,
    from: 0,
    skin: "round",
    onStart: function(data) {
        $input.prop( "value", parseInt(data.from*parseInt(current_balance)/100)) ;
    },
    onChange: function(data) {
        $input.prop("value", parseInt(data.from*parseInt(current_balance)/100) );
    },
    postfix: "%"
});

instance = $range.data("ionRangeSlider");

$input.on("change keyup", function() {
var val = $(this).prop("value");

    // validate
    if (val < min) {
        val = min;
    } else if (val > max) {
        val = max;
    }

    instance.update({
        from: val
    });
});


$(".irs-min").css({"color": "var(--body-color)", "background-color": "var(--card-header)", "border": "1px solid var(--gray-300)"});
$(".irs-max").css({"color": "var(--body-color)", "background-color": "var(--card-header)", "border": "1px solid var(--gray-300)"});
$(".irs-single").css("background-color", "#0db02b");
$(".irs-bar").css("background-color", "#0db02b");
$(".irs-handle").css("border-color", "#0db02b");
