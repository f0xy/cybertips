games = [];

class GameCard {
    constructor (time, img1, team1, img2, team2, league, event_type, event_content, game_id) {
        this.time = time;
        this.img1 = img1;
        this.team1 = team1;
        this.img2 = img2;
        this.team2 = team2;
        this.league = league;
        this.event_type = event_type;
        this.event_content = event_content;
        this.game_id = game_id;
    }

    info() {
      let curr_info = [this.time, this.img1, this.team1, this.img2, this.team2, this.league, this.event_type, this.event_content, this.game_id]
      return curr_info;
    }

    getId() {
        return this.game_id;
    }
}

const GameFactory = {
    makeGame : (time, img1, team1, img2, team2, league, event_type, event_content, game_id) => new GameCard( time, img1, team1, img2, team2, league, event_type, event_content, game_id )
} 


function getLenghtGames(array) {
    let length = 0;
    for(var key in games) {
        if(games.hasOwnProperty(key)){
            length++;
        }
    }
    return length;
}

