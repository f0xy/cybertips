//  js inique
$(document).ready(function () {
    $("body").on('click', '.like-button-unique',function (e) {
        var like_button = $(this);
        console.log(like_button);
        if (like_button.hasClass("mdi-heart-outline")) {
            like_button.removeClass("mdi-heart-outline");
            like_button.addClass("mdi-heart");
            amount_likes = parseInt(like_button.parent().find("#number_likes").text()) + 1;
            console.log(amount_likes);
            like_button.parent().find("#number_likes").text(amount_likes);
        } else {
            like_button.removeClass("mdi-heart");
            like_button.addClass("mdi-heart-outline");
            amount_likes = parseInt(like_button.parent().find("#number_likes").text()) -1;
            console.log(amount_likes);
            like_button.parent().find("#number_likes").text(amount_likes);
        }
        prediction_id = like_button.parent().parent().find("input").val();
        socket.emit('add_like', parseInt(prediction_id));
    });
});

if (localStorage.getItem("theme") != undefined) {
    var current_theme = localStorage.getItem('theme');
    document.documentElement.setAttribute('theme', current_theme);
 } else {
     localStorage.setItem('theme', 'light');
     document.documentElement.setAttribute('theme', 'light');
     current_theme = localStorage.getItem('theme');
 }

const toggleBtn = document.querySelector("#toggle-theme-unique");

toggleBtn.addEventListener('click', (e) => {
    var current_theme = localStorage.getItem('theme');
    if (current_theme == 'light') {;
        document.documentElement.setAttribute('theme', 'dark');
        localStorage.setItem('theme', 'dark');
    } else {
        document.documentElement.setAttribute('theme', 'light');
        localStorage.setItem('theme', 'light');
    }
});
