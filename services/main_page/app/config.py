import os


class Config:
    DEBUG_MODE = os.environ.get('DEBUG_MODE')
    SECRET_KEY = os.environ.get('SECRET_KEY')
    USERS_AVATAR_FOLDER = "app/blueprints/main_page/static/main_page/images/users_avatar"
    
    EXECUTOR_TYPE = 'thread'
    EXECUTOR_MAX_WORKERS = 5
    EXECUTOR_PROPAGATE_EXCEPTIONS = True

    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS')

    CACHE_TYPE = "simple" # Flask-Caching related configs
    CACHE_DEFAULT_TIMEOUT = 300

    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = "noreply@cybertips.net"
    MAIL_PASSWORD = "edzrsemsfplxzfrd"
    MAIL_SERVER = "smtp.yandex.ru"
    MAIL_PORT = 465
    