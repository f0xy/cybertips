from app.database import db
from sqlalchemy import ForeignKey

class Statistic(db.Model): 
    __tablename__ = 'statistics'

    id = db.Column(db.Integer, primary_key= True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable = False)

    month = db.Column(db.Integer, nullable = False)
    year = db.Column(db.Integer, nullable = False)

    amount_predictions = db.Column(db.Integer, nullable=False, default=0)
    profit = db.Column(db.Integer, nullable=False, default=0)

    ROI = db.Column(db.Integer,nullable=False ,default=0)
    successful_predictions = db.Column(db.Integer, nullable=False, default=0)
    
    unsuccessful_predictions = db.Column(db.Integer, nullable=False, default=0)
    stake_amount = db.Column(db.Integer, nullable=False, default=0)

    

    def __repr__(self):
        return "<Statistic(id = '{0}', user_id = '{1}', amount_predictions = '{2}')>".format(self.id, self.user_id, self.amount_predictions)