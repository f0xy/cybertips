from app.database import db
from sqlalchemy import ForeignKey
from sqlalchemy import and_
from sqlalchemy.orm import relationship, backref
from app.database.models.notification import Notification


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key= True)
    email = db.Column(db.String(120), unique=True, nullable = False)

    password = db.Column(db.String(120), nullable = False)
    nickname = db.Column(db.String(120), nullable = False)

    phone = db.Column(db.String(120), unique=True)
    date_of_registration = db.Column(db.DateTime, nullable = False)

    balance = db.Column(db.Float)
    rating = db.Column(db.Float)

    total_earn = db.Column(db.Integer, nullable=False, default=0)
    amount_predictions = db.Column(db.Integer, nullable=False, default=0)

    profit = db.Column(db.Integer, nullable=False, default=0)
    ROI = db.Column(db.Integer, nullable=False, default=0)

    successful_predictions = db.Column(db.Integer, nullable=False, default=0)
    unsuccessful_predictions = db.Column(db.Integer, nullable=False, default=0)
    
    name = db.Column(db.String(120), nullable = True)
    second_name = db.Column(db.String(120), nullable = True)

    description = db.Column(db.String(240), nullable = True)
    confirmed = db.Column(db.Boolean, nullable=False, default=False)

    confirmed_on = db.Column(db.DateTime, nullable=True)
    link_vk = db.Column(db.String(240), nullable = True)

    link_fb = db.Column(db.String(240), nullable = True)
    link_twitter = db.Column(db.String(240), nullable = True)
    
    recover_pass = db.Column(db.Boolean, nullable=False, default=False)
    user_img = db.Column(db.String(240), nullable=False, default='default.svg')

    likes = db.relationship('Like')
    amount_credits = db.Column(db.Integer, nullable=False, default=0)

    def get_notifications(self):
        return Notification.query.filter(Notification.user_id == self.id).order_by(Notification.date.desc()).all()

    def get_notifications_status(self):
        return Notification.query.filter(and_(Notification.user_id == self.id, Notification.viewed == False)).first()

    def get_id(self):
        return self.id

    def is_active(self):
        return True

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def __repr__(self):
        return "<User(id = '{0}', name = '{1}')>".format(self.id, self.nickname)
