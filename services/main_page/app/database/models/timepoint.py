from app.database import db


class TimePoint(db.Model):
    __tablename__ = 'timepoints'

    id = db.Column(db.Integer, primary_key= True)
    point_for = db.Column(db.String(300), nullable = False)
    date = db.Column(db.DateTime, nullable = False)
    used = db.Column(db.Boolean, default=False)
    
    

    def __repr__(self):
        return "<TimePoint(point_for = '{0}', date = '{1}')>".format(self.point_for, self.date)