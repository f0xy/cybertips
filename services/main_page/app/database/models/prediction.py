from app.database import db
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm import backref


class Prediction(db.Model): 
    __tablename__ = 'predictions'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    date_of_prediction = db.Column(db.DateTime)
    liga = db.Column(db.String(200))

    team1 = db.Column(db.String(120))
    team2 = db.Column(db.String(120))

    team1_img = db.Column(db.String(400))
    team2_img = db.Column(db.String(400))

    type_of_prediction = db.Column(db.String(120)) 
    coefficient = db.Column(db.Float)

    stake_amount = db.Column(db.Float)
    game_start_time = db.Column(db.DateTime)

    description = db.Column(db.String(800))
    final_stake = db.Column(db.Float)

    status_id = db.Column(db.Integer, db.ForeignKey('predictionstatuses.index'))
    status = db.relationship('PredictionStatuses')
    
    views = db.relationship('View')
    likes = db.relationship('Like')
    
    user_details = db.relationship('User')
    express = relationship('Express')

    type_id = db.Column(db.Integer, db.ForeignKey('predictiontypes.index'))
    type_ = db.relationship('PredictionTypes')

    comments = db.relationship('Comment')
    game_1x_id = db.Column(db.Integer)
    
    liga_1x_id = db.Column(db.Integer)


    def __repr__(self):
        return "<Prediction(id = '{0}', rating = '{1}', model = '{2}')>".format(self.id, self.user_id, self.team1)
