from app.database import db
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref


class EventsLiga(db.Model):
    __tablename__ = 'eventsliga'

    id = db.Column(db.Integer, primary_key=True)
    liga = db.Column(db.String(200))
    teams = db.relationship("EventsTeams", backref='eventsliga')
    

    def __repr__(self):
        return "<Liga(id = '{0}', liga = '{1}')>".format(self.id, self.liga)