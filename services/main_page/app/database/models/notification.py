from app.database import db
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref


class Notification(db.Model):
    __tablename__ = 'notifications'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    description = db.Column(db.String(300), nullable=False)
    link = db.Column(db.String(120), default='javascript:void(0);')
    viewed = db.Column(db.Boolean, default=False)
    

    def __repr__(self):
        return "<User(id = '{0}', name = '{1}')>".format(self.id, self.nickname)
