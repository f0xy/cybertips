import eventlet
eventlet.monkey_patch()

# Python_____________________
import requests
import json
import re
from datetime import datetime

# Flask__________________________________________
from flask import request
from flask_socketio import SocketIO, send, emit

# DataBase_______________________________________________
from app.database import db
from app.database import r
from app.database.models.user import User
from app.database.models.notification import Notification
from app.database.models.subscribers import Subscribe

from flask_login import LoginManager, login_user, current_user, login_required, logout_user


users = {}

socketio = SocketIO()


@socketio.on('add_user')
def connect(user_id):
    # Сохраняем пользовательские сессии
    r.set(int(user_id), request.sid)
    users.setdefault(int(user_id), request.sid)

    # Выводим DEBUG INFO (Убрать на продакшене)
    redis_users_sid = {int(key): r.get(key) for key in r.scan_iter()}
    # send('DEBUG INFO' +'\nLocal users sids ' + str(users) + '\nRedis users sids ' + str(redis_users_sid))
    send(str(len(redis_users_sid)))


@socketio.on('disconnect')
def disconnect():
    for i in users.keys():
        if request.sid == users[i]:
            del users[i]
            r.delete(i)
            break
    

@socketio.on('subscribe')
def subscribe(data):
    try:
        ssid_to_send = r.get(int(data['send_to']))
    except:
        ssid_to_send = None
    message_from = data['from']
    date = str(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'))
    new_notification = {
        'user_id': data['send_to'],
        'date': date,
        'description': data['from'] + ' подписался на вас',
        'link': f"/account?user={data['send_from_id']}"
    }
    new_subscribe = {
        'subscription_id': data['send_to'],
        'subscriber_id': data['send_from_id']
    }

    requests.post('http://api/api/v1/notifications', data=json.dumps(new_notification), headers={'content-type': 'application/json'}).content.decode('utf-8')
    requests.post('http://api/api/v1/subscribers', data=json.dumps(new_subscribe), headers={'content-type': 'application/json'}).content.decode('utf-8')

    if ssid_to_send:
        emit('notification', {'title': 'Новый подписчик', 'from': message_from, 'date': date, 'description': 'подписался на вас',
        'link': f"/account?user={data['send_from_id']}"}, room=ssid_to_send)


@socketio.on('unsubscribe')
def unsubscribe(data):
    id_to = int(data['send_to'])
    id_from = data['send_from']
    rf = json.dumps({'filters': [
        {'name': 'subscription_id', 'op': 'eq', 'val': id_to},
        {'name': 'subscriber_id', 'op': 'eq', 'val': id_from}
        ]
    })

    requests.delete(f'http://api/api/v1/subscribers?q={rf}', headers={'content-type': 'application/json'}).content.decode('utf-8')


@socketio.on('clear_notifications')
def clear_notifications(data):
    rf = json.dumps({'filters': [{'name': 'user_id', 'op': 'eq', 'val': data}]})
    requests.delete(f'http://api/api/v1/notifications?q={rf}', headers={'content-type': 'application/json'}).content.decode('utf-8')


@socketio.on('viewed')
def viewd(user_id):
    Notification.query.filter_by(user_id = int(user_id)).update(dict(viewed = True))
    db.session.commit()


@socketio.on('add_event')
def add_event(event):
    event_link = re.findall(r'\d{9}', f'{event}')
    event = requests.get(f'https://1xstavka.ru/LineFeed/GetGameZip?id={event_link[-1]}&lng=ru').content.decode('utf-8')
    emit('game_id', event)


@socketio.on('new_prediction')
def new_prediction(data):

    user_id = data['user_id']
    description = data['description']
    link = data['link']

    user = User.query.get(user_id)
    subscribers = Subscribe.query.filter(Subscribe.subscription_id == int(user_id)).all()
    notifications_list = []
    for sub in subscribers:
        notifications_list.append(Notification(user_id=sub.subscriber_id, date=datetime.utcnow(), description=user.nickname + ' ' + description, link=link))
        try:
            ssid_to_send = r.get(sub.subscriber_id)
            if ssid_to_send:
                emit('notification', {'title': 'Новый прогноз', 'from': user.nickname, 'date': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
                'description': description, 'link': link}, room=ssid_to_send)
        except:
            pass
        

    db.session.add_all(notifications_list)
    db.session.commit()


@socketio.on('add_like')
def add_like(prediction_id):
    rf = json.dumps({'filters': [{'name': 'prediction_id', 'op': 'eq', 'val': prediction_id}]})
    likes = json.loads(requests.get(f'http://api/api/v1/likes?q={rf}').content.decode('utf-8'))['objects']
    if current_user.is_authenticated:
        user_id = int(current_user.get_id())
        if len([i for i in likes if int(i['user_id']) == user_id ]) == 0 and prediction_id:
            new_like = {
            'user_id': user_id,
            'prediction_id': prediction_id
            }
            requests.post('http://api/api/v1/likes', data=json.dumps(new_like), headers={'content-type': 'application/json'}).content.decode('utf-8')
        else:
            rf = json.dumps({'filters': [
                {'name': 'prediction_id', 'op': 'eq', 'val': prediction_id},
                {'name': 'user_id', 'op': 'eq', 'val': user_id}
                ]
            })
            requests.delete(f'http://api/api/v1/likes?q={rf}', headers={'content-type': 'application/json'}).content.decode('utf-8')
