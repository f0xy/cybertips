from flask import render_template, url_for, redirect, request, jsonify, flash
from flask_login import LoginManager, login_user, current_user, login_required, logout_user
from werkzeug.security import check_password_hash, generate_password_hash
from app.blueprints.auth import auth
from app.config import Config


from app.database import db
from app.database.models.admin import Admin


admin = Admin.query.filter_by(login='root').first()

if admin:
    if not check_password_hash(Config.ADMIN_ROOT_PASSWORD, admin.password):
        admin.password = generate_password_hash(Config.ADMIN_ROOT_PASSWORD)
        db.session.add(admin)
        db.session.commit()
else:
    admin = Admin(name='root', login='root', password=generate_password_hash(Config.ADMIN_ROOT_PASSWORD))
    db.session.add(admin)
    db.session.commit()


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('admin_panel.index'))

    login = request.form.get('login')
    password = request.form.get('password')
    
    if login and password:
        admin = Admin.query.filter(Admin.login == login).first()
        
        if admin and check_password_hash(admin.password, password):
            login_user(admin, remember=request.form.get('remember'))
            return redirect(url_for('admin_panel.index'))
            

        flash('Неправильный логин или пароль')
        return render_template('login.html')
    
    return render_template('login.html')

@auth.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    if not current_user.is_authenticated:
        return redirect(url_for('auth.login'))
    logout_user()
    return redirect(url_for('auth.login'))