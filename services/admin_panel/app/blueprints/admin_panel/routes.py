import timeit
from datetime import datetime
import json, requests
from flask import render_template, url_for, redirect, request
from flask_login import login_required
from app.blueprints.admin_panel import admin_panel
from app.database import db

from flask import Flask, request, redirect, Response
import requests



@admin_panel.route('/', methods=['GET'])
@login_required
def index():
    return render_template('index.html')


@admin_panel.route('/flower', methods=['GET'])
@login_required
def flower():
    return render_template('index.html')


# @admin_panel.route('/<path:path>', methods=['GET'])
# def proxy(path):
#     if request.method=='GET':
#         resp = requests.get(f'{"http://localhost:5554/"}{path}')
#         excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
#         headers = [(name, value) for (name, value) in resp.raw.headers.items() if name.lower() not in excluded_headers]
#         response = Response(resp.content, resp.status_code, headers)
#     return response
