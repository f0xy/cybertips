moment.locale('ru');
$(document).ready(function(){
    const local_date = $('.local-date');
    $.each(local_date, function(_, value) {
        value.innerHTML = local_time(value.innerHTML);
    });
})

$(document).ready(function(){
    const local_date_exactly = $('.local-date-exactly');
    $.each(local_date_exactly, function(_, value) {
        value.innerHTML = local_time(value.innerHTML, 'exactly');
    });
})

function re_local_notifications() {
    const local_date = $('body').find( $('#notif-block') ).find( $('.local-date-new') );
    $.each(local_date, function(_, value) {
        value.innerHTML = local_time(value.innerHTML);
    });
}


function local_time(date_, param) {
    var date_ = moment.utc(date_).local();
    
    
    var now = moment();
    var time_delta = moment.duration(now.diff(date_));

    if (param == 'exactly') {
        if (date_.year() != now.year()) {
            return date_.format('DD MMM YYYY [в] HH:mm');
        } else {
            return date_.format('DD MMM [в] HH:mm');
        }
    }
    

    if (time_delta.hours() == 0 && time_delta.minutes() == 0) {
        return 'сейчас';
    }

    if (time_delta.hours() == 0 && time_delta.days() == 0) {
        return time_delta.minutes() + ' мин. назад';
    }

    if (time_delta.hours() != 0 && time_delta.days() == 0) {
        return time_delta.hours() + ' ч. назад';
    }

    if (time_delta.days() == 1) {
        return 'вчера в ' + date_.format('HH:mm');
    }

    if (time_delta.days() > 1) {
        if (date_.year() != now.year()) {
            return date_.format('DD MMM YYYY [в] HH:mm');
        } else {
            return date_.format('DD MMM [в] HH:mm');
        }
        
    }
}