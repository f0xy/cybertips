from app.database import db


class Admin(db.Model):
    __tablename__ = 'admins'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    login = db.Column(db.String(120), nullable=False, unique=True)
    password = db.Column(db.String(140), nullable=False)

    def get_id(self):
        return self.id

    def is_active(self):
        return True

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def __repr__(self):
        return "<Admin(id = '{0}', name = '{1}', login = '{2}')>".format(self.id, self.name, self.login)
