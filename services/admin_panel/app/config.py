import os


class Config:
    DEBUG_MODE = os.environ.get('DEBUG_MODE')
    SECRET_KEY = os.environ.get('SECRET_KEY')
    ADMIN_ROOT_PASSWORD = os.environ.get('ADMIN_ROOT_PASSWORD')
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS')
